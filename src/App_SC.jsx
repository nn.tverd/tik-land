import styled from "styled-components";
//==============================================================================

export const AppWrap = styled.div`
    min-height: 100%;
    height: 100%;
    min-width: 100%;

    width: 100%;
    
    background-color: rgb(20, 20, 20);
    color: white;
    display: flex   ;
    justify-content: center;
    background-image: url("images/turn-phone.png");
    background-size: 25%;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
        display: none;
    }
    overflow: scroll;
    scroll-snap-type: y mandatory;
`;

export const ViewPort = styled.div`
    min-height: 345px;
    height: ${(props) => props.sizeOfContent.height}px;
    max-height: 100%;

    min-width: 200px;
    width: ${(props) => props.sizeOfContent.width}px;

    position: relative;
    background-color: black;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &:-webkit-scrollbar {
        display: none;
    }
`;

