// https://www.npmjs.com/package/react-dynamic-font
// https://github.com/zhihu/griffith

import React, { useEffect, useRef } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import { Provider } from "react-redux"; // import { css, jsx } from '@emotion/react'
import { observer } from "mobx-react-lite";
import { deviceDetect, isIOS, isAndroid, getUA } from "react-device-detect";
import { SnackbarProvider } from "notistack";

//==============================================================================
// import useDebounce from "./Components/Debounce.js";
//==============================================================================

import "./App.css";
import "./Components/css/common.css";
//==============================================================================
// import ContentView from "./Components/_4_wrappers/ContentView_Cnt.js";
// import DebuggerView from "./Components/_4_wrappers/DebuggerView.jsx";
import AppWrapper from "./Components/_4_wrappers/AppWrapper_Cmp.jsx";
// import LoginPage from "./Components/_5_admin/LoginPage.jsx";
// import AdminWrapper from "./Components/_5_admin/AdminWrapper";
// import ProtectedRoute from "./Components/_5_admin/ProtectedRoute";
// import EmailNotification from "./Components/_5_admin/EmailNotification";
// import ResetVerify from "./Components/_5_admin/ResetVerify";
// import SignUpVerify from "./Components/_5_admin/SignUpVerify";

//==============================================================================
import ViewPortSizeStore from "./store/ViewPortSizeStore.js";
import DebuggerStore from "./store/DebuggerStore.js";
import SystemStore from "./store/SystemStore.js";

//==============================================================================
import { AppWrap, ViewPort } from "./App_SC.jsx";
//==============================================================================
// import conf from "./data/cf.js";
//==============================================================================

//==============================================================================
//==============================================================================

// const pages = conf.pages;

function App() {
    const { setSizeViewPort } = ViewPortSizeStore;
    const viewportRef = useRef(null);
    const getGeoDetails = () => {
        fetch(
            "https://geolocation-db.com/json/c0593a60-4159-11eb-80cd-db15f946225f"
        )
            .then((response) => response.json())
            .then((geoData) => SystemStore.setGeo(geoData));
    };

    useEffect(() => {
        setSizeViewPort(window.innerWidth, window.innerHeight);
        getGeoDetails();
        SystemStore.setUserAgent(getUA);
        SystemStore.setDevice(deviceDetect());
        SystemStore.setIsIOS(isIOS /*|| isAndroid */);
        // SystemStore.setIsIOS(isIOS);
        window.addEventListener("resize", () => {
            setSizeViewPort(window.innerWidth, window.innerHeight);
        });
    }, []);
    // DebuggerStore.setDebuggerValue(SystemStore.deviceData);
    DebuggerStore.setDebuggerValue(getUA);

    return (
        // <div className="app" css={css`

        <AppWrap ref={viewportRef}>
            {/* <Provider store={store}> */}
            <SnackbarProvider maxSnack={7}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/:landingId">
                            <ViewPort
                                sizeOfContent={
                                    ViewPortSizeStore.sizeOfContentView
                                }
                            >
                                <AppWrapper />
                            </ViewPort>
                        </Route>
                    </Switch>
                </BrowserRouter>
            </SnackbarProvider>
            {/* </Provider> */}
        </AppWrap>
    );
}

export default observer(App);
