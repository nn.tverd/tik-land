import { makeAutoObservable } from "mobx";

class ViewPortSizeStore {
    widthOfViewPort = 720;
    heightOfViewPort = 1280;
    __ratio = 1.777777777;

    constructor() {
        makeAutoObservable(this);
    }

    setWidthOfViewPort = (width) => {
        this.widthOfViewPort = width;
    };

    setHeightOfViewPort = (height) => {
        this.heightOfViewPort = height;
    };
    setSizeViewPort = (width, height) => {
        // // console.log("setSizeViewPort", width, height);
        this.widthOfViewPort = width;
        this.heightOfViewPort = height;
    };

    get sizeOfContentView() {
        const width = this.widthOfViewPort;
        const height = this.heightOfViewPort;
        const ratio = height / width;
        // alert(ratio);

        if (1.2 < ratio && ratio < 2.2) {
            // // console.log("if( 1,6 < ratio && ratio< 1,8 ){", ratio)
            return { width: width, height: height };
        }
        const calculatedWidth = Math.round(height / this.__ratio) - 1;
        const calculatedHeight = Math.round(width * this.__ratio) - 1;
        // // console.log("calculation", width, height, calculatedWidth, calculatedHeight, typeof height, typeof width, type);
        if (calculatedHeight < height) {
            return { width: width, height: calculatedHeight };
        } else {
            return { width: calculatedWidth, height: height };
        }
    }
}

export default new ViewPortSizeStore();
