import { makeAutoObservable } from "mobx";

class VideoControlsStore {
    showGlobalCovers = true;
    // releaseGlobalCover= true;
    globalPlayEnabled = false;
    videoUrl = "";
    // lockChangeVideo = false;

    muted = true;
    controlCanBeUnmuted = false;
    controlCanBeUnmutedTimer = null;
    canBeUnmuted = -1;

    // 1 onPlay ставим muted=false
    // 1 controling 5 sec
    // 2 onPause (globalPlayEnabled && controlling ) show message

    constructor() {
        makeAutoObservable(this);
    }

    setMuted = (m) => {
        return (this.muted = m);
    };
    setControlCanBeUnmuted = (c) => {
        this.controlCanBeUnmuted = c;
    };
    setControlCanBeUnmutedTimer = (t) => {
        this.controlCanBeUnmutedTimer = t;
    };
    setCanBeUnmuted = (can) => {
        // 0 - can't
        // 1 can
        // -1 unset
        this.canBeUnmuted = can;
    };

    // setLockChangeVideo = (lock) => {
    //     this.lockChangeVideo = lock;
    // };
    setVideoUrl = (videoUrl) => {
        this.videoUrl = videoUrl;
        // console.log( "setVideoUrl = (videoUrl) => {", videoUrl )
    };
    setShowGlobalCovers = (showGlobalCovers) => {
        this.showGlobalCovers = showGlobalCovers;
    };
    setGlobalPlayEnabled = (globalPlayEnabled) => {
        this.globalPlayEnabled = globalPlayEnabled;
    };
}

export default new VideoControlsStore();
