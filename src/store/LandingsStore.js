import { makeAutoObservable } from "mobx";
const shortId = require("shortid");

class LandingsStore {
    landing = null;
    landingsLoading = false;
    error = "";
    message = "";

    hasMore = true;
    lastLoadedPage = 0;
    selectedLending = null;
    totalPages = 0;
    totalArray = [];
    constructor() {
        makeAutoObservable(this);
    }

    setSelected = (l) => {
        this.selectedLending = l;
    };

    getLandingById = async (id) => {
        
        const url = process.env.REACT_APP_SERVER_BASE_URL + "landing/get/" + id;
        // console.log(url, process.env)
        const resp = await fetch(url);
        // console.log(resp)

        const response = await resp.json();
        // console.log(response);
        if (resp.status === 200) {
            this.setSelected(response.payload.landing);
            return response.payload.pages;
        }
        return [];
    };

    getLandingByIdPagination = async (id, offset, limit) => {
        await setTimeout(()=>{}, 5000)
        if (this.hasMore) {
            if (offset < this.lastLoadedPage) return [];
            const url =
                process.env.REACT_APP_SERVER_BASE_URL + "landing/getPage";
            const resp = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    landingId: id,
                    limit: 2,
                    offset: this.lastLoadedPage,
                }),
            });
            const response = await resp.json();
            if (response.error) {
                // this.error = response.error;
                console.log(this.error);

                return [];
            }
            if (response.message) {
                // this.message = response.message;
                // console.log(this.message, response.message, response.payload);
                this.hasMore = response.payload.hasMore;
                this.lastLoadedPage = response.payload.lastLoaded;
                this.setSelected(response.payload.landing);
                this.totalArray = new Array(response.payload.total);
                this.totalPages = response.payload.total;
                // console.log(
                //     "response.payload.slicedPages",
                //     response.payload.slicedPages
                // );

                return response.payload.slicedPages;
            }
            // console.log("last return");
            return [];
        }
    };

    get showError() {
        if (!this.error) return null;
        const _error = `Error # - ${shortId.generate()}: ${this.error}`;
        // this.error = "";
        return _error;
    }
    get showMessage() {
        if (!this.message) return null;

        const _message = `Message # - ${shortId.generate()}: ${this.message}`;
        // this.message = "";
        return _message;
    }
}

export default new LandingsStore();
