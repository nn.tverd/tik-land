import { makeAutoObservable } from "mobx";

class AdminAuthStore {
    isAuth = false;
    token = "";
    user = {};
    constructor() {
        makeAutoObservable(this);
    }

    setUser = (user) => {
        this.user = user;
    };

    saveToken = (tok) => {
        this.token = tok;
    };
    changeAuth = (isAuth) => {
        this.isAuth = isAuth;
    };

    updateUserInfo = async () => {
        const resp = await fetch(process.env.REACT_APP_SERVER_BASE_URL + "user/updateUserInfo", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": this.token,
            },
            body: JSON.stringify({ userId: this.user._id }),
        });
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            return;
        }
        if (response.message) {
            this.message = response.message;

            this.user = response.user;
        }
    };
}

export default new AdminAuthStore();
