import { makeAutoObservable } from "mobx";
import VideoControlStore from "./VideoControlsStore";

// import conf from "../data/cf.js";
const shortId = require("shortid");

class PagesStore {
    pages = [];
    shownIndex = null;
    numOfPagesWereLoad = 0;
    pagesLoading = false;
    error = "";
    message = "";

    constructor(rootStore) {
        // console.log("VideoControlStore in page store", VideoControlStore);
        makeAutoObservable(this);
    }

    getListOfPages = async ({ userId, landingId, token }) => {
        // getListOfPages
        this.pagesLoading = true;
        const resp = await fetch(
            process.env.REACT_APP_SERVER_BASE_URL + "page/getListOfPages",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": token,
                },
                body: JSON.stringify({ userId: userId, landingId: landingId }),
            }
        );
        this.pagesLoading = false;
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            // console.log(this.error);
            return;
        }
        this.pages = response.payload;
        if (response.message) {
            this.message = response.message;
            // console.log(this.message, response.message);
        }
    };

    createNewPage = async ({ userId, landingId, token }) => {
        this.pagesLoading = true;
        // console.log(landingId);
        const resp = await fetch(
            process.env.REACT_APP_SERVER_BASE_URL + "page/createPage",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": token,
                },
                body: JSON.stringify({ userId: userId, landingId: landingId }),
            }
        );
        this.pagesLoading = false;
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            // console.log(this.error);
            return;
        }
        this.getListOfPages({ userId, landingId, token });
        if (response.message) {
            this.message = response.message;
            // console.log(this.message, response.message);
        }
    };

    updateLikes = async (landingId) => {
        const resp = await fetch(
            process.env.REACT_APP_SERVER_BASE_URL + "page/incrementLikes",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ landingId: landingId }),
            }
        );
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            console.log(this.error);
            return;
        }
        if (response.message) {
            this.message = response.message;
            console.log(this.message, response.message);
            return;
        }
    };

    updateShares = async (landingId) => {
        const resp = await fetch(
            process.env.REACT_APP_SERVER_BASE_URL + "page/incrementShares",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ landingId: landingId }),
            }
        );
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            console.log(this.error);
            return;
        }
        if (response.message) {
            this.message = response.message;
            console.log(this.message, response.message);
            return;
        }
    };

    incShownIndex = () => {
        this.shownIndex = this.checkLimits(this.shownIndex + 1);
    };

    setShownIndex = (index) => {
        this.shownIndex = this.checkLimits(index);
    };

    checkLimits(newShownIndex) {
        if (newShownIndex < 0) {
            return 0;
        }
        if (newShownIndex >= this.pages.length) {
            if (this.pages.length > 0) {
                return this.pages.length - 1;
            } else {
                return 0;
            }
        }
        return newShownIndex;
    }

    getPages = () => {
        // console.log("this.numOfPagesWereLoad", this.numOfPagesWereLoad);
        // this.pages = [...conf.pages];
        // this.numOfPagesWereLoad = this.pages.length;
    };

    setPages = (pages) => {
        console.log(pages);
        this.pages = [...pages];
        this.numOfPagesWereLoad = this.pages.length;
    };
    setPages2 = (pages) => {
        if (!pages) return this.pages;
        pages.forEach((page) => {
            page.paused = 1;
            // page.lockChanges = false;
        });
        if (!this.shownIndex) {
            this.shownIndex = pages[0]._id;
        }

        this.pages = [...this.pages, ...pages];
        this.numOfPagesWereLoad = this.pages.length;
        // console.log("setPages2", this.pages.length);
        return this.pages;
    };
    // changeLockChanges = (page, status) => {
    //     page.lockChanges = status;
    // };
    setAllPagesPaused = (shownId) => {
        this.pages.forEach((page) => {
            if (page._id === shownId) {
                if (VideoControlStore.globalPlayEnabled) {
                    page.paused = 0;
                } else {
                    page.paused = 1;
                }
            } else {
                page.paused += 1;
            }
        });
        for (let i in this.pages) {
            // console.log(this.pages[i]._id, "  - - - -> ", this.pages[i].paused);
        }
    };

    get showError() {
        if (!this.error) return null;
        const _error = `Error # - ${shortId.generate()}: ${this.error}`;
        // this.error = "";
        return _error;
    }
    get showMessage() {
        if (!this.message) return null;

        const _message = `Message # - ${shortId.generate()}: ${this.message}`;
        // this.message = "";
        return _message;
    }
}

export default new PagesStore();
// export default PagesStore;
