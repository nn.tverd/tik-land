const pagesStore = require("./PagesStore.js");

// global parent store
class Store {
    pageStore = null;
    constructor() {
        this.pageStore = new pagesStore(this);
        // this.userStore = new UserStore(this);
        // ... another stores
    }
}
