import { makeAutoObservable } from "mobx";

class SystemStore {
    geoData = {};
    deviceData = {};
    isIOS = false;
    userAgent = null;
    shouldBeMuted = false;
    clickedOnUnmute = false;
    pausedOnClickedOnUnmute = false;
    showMutedDialog = false;
    mutedDialogWasShownAndClosedByUser = false;

    constructor() {
        makeAutoObservable(this);
    }

    setGeo = (obj) => {
        this.geoData = obj;
    };

    setDevice = (obj) => {
        this.deviceData = obj;
    };

    setIsIOS = (isIOS) => {
        // console.log(isIOS);
        this.isIOS = isIOS;
        // this.isIOS = true; // ios development
        // this.isIOS = false; // other development
    };
    resetShouldBeMuted = () => {
        this.shouldBeMuted = false;
    };
    setUserAgent = (ua) => {
        this.userAgent = ua;
        const agent = ua.toLowerCase();
        if (agent.includes("instagram")) this.shouldBeMuted = true;
        if (agent.includes("fban")) this.shouldBeMuted = true;
        if (agent.includes("[fb")) this.shouldBeMuted = true;
        this.showMutedDialog = this.shouldBeMuted;
    };

    SetMutedDialogWasShownAndClosedByUser = () => {
        this.mutedDialogWasShownAndClosedByUser = true;
    }
}

export default new SystemStore();
