import { makeAutoObservable } from "mobx";

class DebuggerStore {
    DebuggerString = "debugger";
    DebuggerArray = [];
    constructor() {
        makeAutoObservable(this);
    }

    setDebuggerValue = (obj) => {
        this.DebuggerString = JSON.stringify(obj, 0, 5);
    };
    pushDebugger = (obj) => {
        this.DebuggerArray.push(JSON.stringify(obj, 0, 5));
    };
}

export default new DebuggerStore();
