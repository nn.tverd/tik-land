import { makeAutoObservable } from "mobx";
// import conf from "../data/cf.js";
const shortId = require("shortid");

class PagesStore {
    pages = [];
    selectedPageId = null;

    muteAllPages = false;
    playVideoOnPage = false;
    constructor() {
        makeAutoObservable(this);
    }
    changeSelectedPage = (id) => {
        return (this.selectedPageId = id);
    };

    saveParticularPage = async (token) => {
        const page = this.pages.find(
            (page) => page._id === this.selectedPageId
        );
        if (!page) {
            alert("Proplem to save page. Page did not found");
        }
        const { user, landing, _id } = page;
        this.pagesLoading = true;
        const resp = await fetch(
            process.env.REACT_APP_SERVER_BASE_URL + "page/saveParticularPage",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": token,
                },
                body: JSON.stringify({
                    userId: user,
                    landingId: landing,
                    pageId: _id,
                    updatedPage: page,
                }),
            }
        );
        this.pagesLoading = false;
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            return;
        }
        this.pages = response.payload;
        this.selectedPageId = response.payload[0]._id;
        if (response.message) {
            this.selectedPageId = null;
            this.message = response.message;
        }
    };

    getListOfPages = async ({ userId, landingId, token }) => {
        // getListOfPages
        this.pagesLoading = true;
        const resp = await fetch(process.envSERVER_BASE_URL+"page/getListOfPages", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": token,
            },
            body: JSON.stringify({ userId: userId, landingId: landingId }),
        });
        this.pagesLoading = false;
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            return;
        }
        this.pages = response.payload;
        // this.selectedPageId = response.payload[0]._id;
        if (response.message) {
            this.message = response.message;
        }
    };

    createNewPage = async ({ userId, landingId, token }) => {
        this.pagesLoading = true;
        const resp = await fetch(process.env.REACT_APP_SERVER_BASE_URL + "page/createPage", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": token,
            },
            body: JSON.stringify({ userId: userId, landingId: landingId }),
        });
        this.pagesLoading = false;
        const response = await resp.json();
        if (response.error) {
            this.error = response.error;
            return;
        }
        this.getListOfPages({ userId, landingId, token });
        if (response.message) {
            this.message = response.message;
        }
    };

    incShownIndex = () => {
        this.shownIndex = this.checkLimits(this.shownIndex + 1);
    };

    setMuteAllPages = () => {
        this.muteAllPages = !this.muteAllPages;
    };

    setPlayVideoOnPage = () => {
        this.playVideoOnPage = !this.playVideoOnPage;
    };

    get showError() {
        if (!this.error) return null;
        const _error = `Error # - ${shortId.generate()}: ${this.error}`;
        // this.error = "";
        return _error;
    }
    get showMessage() {
        if (!this.message) return null;

        const _message = `Message # - ${shortId.generate()}: ${this.message}`;
        // this.message = "";
        return _message;
    }
}

export default new PagesStore();
