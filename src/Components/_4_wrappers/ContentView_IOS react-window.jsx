import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";
import { useParams } from "react-router-dom";
// import InfiniteScroll from "react-infinite-scroll-component";

import { FixedSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import InfiniteLoader from "react-window-infinite-loader";
// ====================================================================================
// styled components
import {
    VideoWrapper,
    ContentViewCont,
    ScrollWrap,
} from "./ContentView_SC.jsx";
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
import VideoControlsStore from "../../store/VideoControlsStore.js";
import PagesStore from "../../store/PagesStore.js";
import LandingsStore from "../../store/LandingsStore.js";
import ViewPortSizeStore from "../../store/ViewPortSizeStore.js";

// ====================================================================================
// ====================================================================================
// ====================================================================================
import CardComponent from "../_3_chunks/Card__Cmp.jsx";
// ====================================================================================
// *********************************************

// const LOADING = 1;
// const LOADED = 2;
// let itemStatusMap = {};
// *********************************************
// *********************************************
const Row = (props) => {
    const { index, style } = props;
    const page = PagesStore.pages[index];
    if (!page) return null;
    // console.log("ROW", page);
    const shownIndex = PagesStore.shownIndex;
    return (
        <div
            className="ListItem"
            style={{
                ...style,
                backgroundColor: "red",
                // scrollSnapAlign: "start",
                // scrollSnapStop: "always",
                // webkitScrollSnapAlign: "start",
            }}
        >
            {/* 123 {index} */}
            <CardComponent
                key={index}
                myIndex={index}
                page={page}
                shownIndex={shownIndex}
            />
        </div>
    );
};

export default observer(function ContentView(props) {
    const { landingId } = useParams();
    const {
        videoUrl,
        globalPlayEnabled,
        setShowGlobalCovers,
    } = VideoControlsStore;
    const { pages, shownIndex } = PagesStore;
    const { selectedLending } = LandingsStore;
    const VideoComponent = (
        <VideoWrapper>
            <ReactPlayer
                width="100%"
                height="120%"
                url={videoUrl}
                controls={false}
                muted={false}
                loop={true}
                playsinline={true}
                playing={globalPlayEnabled}
                onPlay={() => setShowGlobalCovers(false)}
                onPause={() => setShowGlobalCovers(true)}
                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            modestbranding: 1,
                            showinfo: 0,
                            origin: "https://candylanding.netlify.app/",
                            enablejsapi: 1,
                        },
                    },
                }}
            />
        </VideoWrapper>
    );
    let newarray = [];
    if (selectedLending) {
        newarray = selectedLending.pages
            .slice()
            .sort((a, b) => a.order > b.order);
        // console.log(newarray);
    }

    const isItemLoaded = (index) => !!PagesStore.pages[index];

    // useEffect(() => {
    //     LandingsStore.getLandingByIdPagination(landingId, 0, 3).then(
    //         (pages) => {
    //             return PagesStore.setPages2(pages);
    //         }
    //     );
    // }, []);

    const loadMoreItems = (startIndex, stopIndex) => {
        console.log("const loadMoreItems");
        LandingsStore.getLandingByIdPagination(
            landingId,
            startIndex,
            stopIndex
        ).then((pages) => {
            return PagesStore.setPages2(pages);
        });
    };

    // console.log(pages);
    return (
        <ContentViewCont id="scrollableDiv" style={{ color: "white" }}>
            {/* <ScrollWrap > */}

            <AutoSizer
                id="aautosizer"
                style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                }}
            >
                {({ height, width }) => {
                    // console.log("height, width = ", height, width);
                    return (
                        <InfiniteLoader
                            id="InfiniteLoader"
                            isItemLoaded={isItemLoaded}
                            itemCount={64}
                            loadMoreItems={loadMoreItems}
                        >
                            {({ onItemsRendered, ref, style }) => {
                                // console.log(
                                //     "heightref, widthref = ",
                                //     height,
                                //     width
                                // );

                                return (
                                    <List
                                        id="ListListListList"
                                        height={height}
                                        width={width}
                                        itemCount={64}
                                        // itemCoun
                                        itemSize={height}
                                        onItemsRendered={onItemsRendered}
                                        ref={ref}
                                        style={{
                                            scrollbarWidth: "none",
                                            msOverflowStyle: "none",
                                            WebkitScrollSnapType: "y mandatory",
                                            scrollSnapType: "y mandatory",
                                            WebkitOverflowScrolling:"touch"
                                        }}
                                        className="hideScroll"

                                        // {...otherListProps}
                                    >
                                        {Row}
                                    </List>
                                );
                            }}
                        </InfiniteLoader>
                    );
                }}
            </AutoSizer>
            {/* {pages &&
                        newarray &&
                        newarray.map((__page, index) => {
                            console.log(__page._id);
                            const page = pages.find(
                                (item) => item._id === __page._id
                            );
                            if (page)
                                return (
                                    <CardComponent
                                        key={index}
                                        myIndex={index}
                                        page={page}
                                        likes={1}
                                        shares={2}
                                        shownIndex={shownIndex}
                                    />
                                );
                            else return null;
                        })} */}

            {/* </ScrollWrap> */}
            {SystemStore.isIOS && VideoComponent}
        </ContentViewCont>
    );
});
