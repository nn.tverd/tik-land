import React from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";
import InfiniteScroll from "react-infinite-scroller";
// import InfiniteScroll from 'react-infinite-scroll-component';
// --- https://github.com/bvaughn/react-virtualized/blob/master/docs/creatingAnInfiniteLoadingList.md
// https://www.npmjs.com/package/react-infinite-scroller
// ====================================================================================
// styled components
import {
    VideoWrapper,
    ContentViewCont,
    ScrollWrap,
} from "./ContentView_SC.jsx";
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
import VideoControlsStore from "../../store/VideoControlsStore.js";
import PagesStore from "../../store/PagesStore.js";
import LandingsStore from "../../store/LandingsStore.js";
// ====================================================================================
// ====================================================================================
// ====================================================================================
import CardComponent from "../_3_chunks/Card__Cmp.jsx";
import { useParams } from "react-router-dom";
// ====================================================================================
// *********************************************

// *********************************************
// *********************************************

export default observer(function ContentView(props) {
    const { landingId } = useParams();
    const {
        videoUrl,
        globalPlayEnabled,
        setShowGlobalCovers,
    } = VideoControlsStore;
    const { pages, shownIndex } = PagesStore;
    const { selectedLending } = LandingsStore;
    const VideoComponent = (
        <VideoWrapper>
            <ReactPlayer
                width="100%"
                height="120%"
                url={videoUrl}
                controls={false}
                muted={false}
                loop={true}
                playsinline={true}
                playing={globalPlayEnabled}
                onPlay={() => setShowGlobalCovers(false)}
                onPause={() => setShowGlobalCovers(true)}
                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            modestbranding: 1,
                            showinfo: 0,
                            origin: "https://candylanding.netlify.app/",
                            enablejsapi: 1,
                        },
                    },
                }}
            />
        </VideoWrapper>
    );
    let newarray = [];
    if (selectedLending) {
        newarray = selectedLending.pages
            .slice()
            .sort((a, b) => a.order > b.order);
        // console.log(newarray);
    }

    const loadMoreItems = (startIndex, stopIndex) => {
        console.log("const loadMoreItems");
        LandingsStore.getLandingByIdPagination(
            landingId,
            startIndex,
            stopIndex
        ).then((pages) => {
            console.log("pages.length", pages.length);
            return PagesStore.setPages2(pages);
        });
    };

    return (
        <ContentViewCont>
            <ScrollWrap>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={loadMoreItems}
                    hasMore={LandingsStore.hasMore}
                    useWindow={false}
                    threshold={1000}
                    loader={
                        <div className="loader" key={0}>
                            Loading ...
                        </div>
                    }
                >
                    {pages &&
                        pages.map((page, index) => {
                            if (page)
                                return (
                                    <CardComponent
                                        key={index}
                                        myIndex={index}
                                        page={page}
                                        shownIndex={shownIndex}
                                    />
                                );
                            else return null;
                        })}
                </InfiniteScroll>
            </ScrollWrap>
            {/* <ScrollWrap>
                {pages &&
                    newarray &&
                    newarray.map((__page, index) => {
                        // console.log(__page._id);
                        const page = pages.find(
                            (item) => item._id === __page._id
                        );
                        if (page)
                            return (
                                <CardComponent
                                    key={index}
                                    myIndex={index}
                                    page={page}
                                    likes={1}
                                    shares={2}
                                    shownIndex={shownIndex}
                                />
                            );
                        else return null;
                    })}
            </ScrollWrap> */}
            {SystemStore.isIOS && VideoComponent}
        </ContentViewCont>
    );
});
