import React, { useState } from "react";
import styled from "styled-components";
import qs123123 from "qs";
import { useLocation } from "react-router-dom";

import DebuggerStore from "../../store/DebuggerStore.js";

export const DebugContainerSmall = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    min-height: 30px;
    height: 30px;
    background-color: rgba(123, 0, 0, 0.5);
    scrollbar-width: none;

    max-width: 100%;
    width: 100%;
    min-width: 100%;
    display: grid;
    place-items: center;
    z-index: 10000;
`;
export const DebugContainerBig = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    min-height: 100%;
    height: 100%;
    background-color: rgba(123, 0, 0, 0.5);
    scrollbar-width: none;

    max-width: 100%;
    width: 100%;
    min-width: 100%;
    display: grid;
    place-items: center;
    z-index: 10000;
`;

export default function DebuggerView(props) {
    const location = useLocation();
    const qstring = location.search;

    const qsParsed = qs123123.parse(qstring.replace("?", ""));
    const debugShow = qsParsed.debug;

    const [fullheight, setFullHeight] = useState(false);
    // const data = useSelector((state) => state.system.debugInfo);
    // DebuggerStore.setDebuggerValue( {x:123})
    if (debugShow) {
        if (fullheight)
            return (
                <DebugContainerBig>
                    <div>
                        <button onClick={() => setFullHeight(!fullheight)}>
                            show
                        </button>
                        <pre>{DebuggerStore.DebuggerString}</pre>
                        <div>
                            {DebuggerStore.DebuggerArray &&
                                DebuggerStore.DebuggerArray.map(
                                    (item, index) => {
                                        <pre key={index}>{item}</pre>;
                                    }
                                )}
                        </div>
                    </div>
                </DebugContainerBig>
            );
        else
            return (
                <DebugContainerSmall>
                    <button onClick={() => setFullHeight(!fullheight)}>
                        show
                    </button>
                </DebugContainerSmall>
            );
    } else return null;
}

// npm i qs --save
