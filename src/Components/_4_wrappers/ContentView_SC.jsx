import styled from "styled-components";
// *********************************************
// *********************************************
export const ContentViewCont = styled.div`
    position: absolute;
    z-index: 5;

    min-height: 100%;
    height: 100%;

    max-height: 100%;
    min-width: 100%;
    width: 100%;
    max-width: 100%;
    overflow: hidden;
    // background-color: tomato;
    scrollbar-width: none;
    border-radius: 10px;

    display: grid;
    place-items: center;
`;
export const VideoWrapper = styled.div`
    margin-top: ${(props) => Math.round(props.offsetY__)}px;
    position: absolute;
    height: 100%;
    width: 100%;
    display: grid;
    place-items: center;
    z-index: -1;
`;
export const Debugggger = styled.div`
    position: fixed;
    z-index: 10000;
    // background-color: rgba(0,0,200,0.5);
    background-color: yellow;

    width: 100%;
    color: black;
`;

export const ScrollWrap = styled.div`
    position: relative;
    // background-color: yellow;

    min-height: 100%;
    height: 100%;
    max-height: 100%;

    min-width: 100%;
    width: 100%;
    max-width: 100%;
    overflow: scroll;
    scroll-snap-type: y mandatory;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
        display: none;
    }
`;