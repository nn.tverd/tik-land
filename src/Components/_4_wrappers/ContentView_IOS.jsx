import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";
// ====================================================================================
// styled components
import {
    VideoWrapper,
    ContentViewCont,
    ScrollWrap,
} from "./ContentView_SC.jsx";
import MessageMuted from "../_2_molecules/MessageMuted.jsx"
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
import VideoControlsStore from "../../store/VideoControlsStore.js";
import PagesStore from "../../store/PagesStore.js";
import LandingsStore from "../../store/LandingsStore.js";
import DebuggerStore from "../../store/DebuggerStore.js";

// import awayGif from "away.gif";

// ====================================================================================
// ====================================================================================
// ====================================================================================
import CardComponent from "../_3_chunks/Card__Cmp.jsx";
import { useParams } from "react-router-dom";

// ====================================================================================
// *********************************************

// *********************************************
// *********************************************

export default observer(function ContentView(props) {
    const { landingId } = useParams();
    const {
        videoUrl,
        globalPlayEnabled,
        setShowGlobalCovers,
    } = VideoControlsStore;
    // const [firstPlay, setFirstPlay] = useState(false);
    const [CanShowMutedMessage, setCanShowMutedMessage] = useState(false);

    const { pages, shownIndex } = PagesStore;
    const { selectedLending, totalArray } = LandingsStore;
    DebuggerStore.pushDebugger("IOS Videos: ");
    DebuggerStore.pushDebugger(videoUrl);
    DebuggerStore.pushDebugger(globalPlayEnabled);

    const VideoComponent = (
        <VideoWrapper>
            <ReactPlayer
                width="100%"
                height="120%"
                url={videoUrl}
                controls={false}
                muted={SystemStore.shouldBeMuted}
                loop={true}
                playsinline={true}
                playing={globalPlayEnabled}
                onPlay={() => {
                    setTimeout(() => {
                        setCanShowMutedMessage(true);
                    }, 3000);
                    setShowGlobalCovers(false);
                }}
                onPause={() => setShowGlobalCovers(true)}
                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            modestbranding: 1,
                            showinfo: 0,
                            origin: "https://candylanding.netlify.app/",
                            enablejsapi: 1,
                        },
                    },
                }}
            />
        </VideoWrapper>
    );
    let newarray = [];
    if (selectedLending) {
        newarray = selectedLending.pages
            .slice()
            .sort((a, b) => a.order > b.order);
        // console.log(newarray);
    }
    useEffect(() => {
        loadMoreItems();
    }, []);

    useEffect(() => {
        if (!pages) return;
        if (!pages.length) return;
        if (shownIndex === pages[pages.length - 1]._id) loadMoreItems();

        if (shownIndex) {
            const _p = pages.find((a) => a._id === shownIndex);
            VideoControlsStore.setVideoUrl(_p.videoUrl);
        }
        PagesStore.setAllPagesPaused(shownIndex);
    }, [shownIndex]);

    const loadMoreItems = () => {
        // console.log("const loadMoreItems");

        LandingsStore.getLandingByIdPagination(landingId).then((__pages) => {
            if (!__pages) return PagesStore.setPages2([]);
            if (!__pages[0]) {
                return PagesStore.setPages2([]);
            }
            return PagesStore.setPages2(__pages);
        });
    };

    // const refs = pages.reduce((acc, value) => {
    //     acc[value.id] = React.createRef();
    //     return acc;
    // }, {});

    // const handleAutoScrollOnLoading = () => {
    //     if (!pages) return;
    //     if (!pages.length) return;
    //     const id = pages[pages.length - 1]._id;
    //     console.log("const handleAutoScrollOnLoading = (id) => {", id);
    //     if (!refs) return;
    //     if (!refs[id]) return;
    //     if (!refs[id].current) return;
    //     console.log(
    //         "const handleAutoScrollOnLoading = (id) => {",
    //         "- - - - - >",
    //         "scroll to view"
    //     );

    //     refs[id].current.scrollIntoView({
    //         behavior: "smooth",
    //         block: "start",
    //     });
    // };

    return (
        <ContentViewCont>
            <ScrollWrap>
                {totalArray &&
                    totalArray.map((__page, index) => {
                        // console.log(__page._id);
                        let page = {
                            empty: true,
                        };
                        if (pages[index]) page = pages[index];
                        // else return null;
                        // console.log("mapping - - - - >", page);

                        if (page)
                            return (
                                <CardComponent
                                    key={index}
                                    // myIndex={index}
                                    page={page}
                                    setCanShowMutedMessage={
                                        setCanShowMutedMessage
                                    }
                                    shownIndex={shownIndex}
                                    // ref={refs[page._id]}
                                    // handleAutoScrollOnLoading={
                                    //     handleAutoScrollOnLoading
                                    // }
                                />
                            );
                        else return null;
                    })}
            </ScrollWrap>
            {SystemStore.isIOS && VideoComponent}
            {SystemStore.showMutedDialog &&
                !SystemStore.mutedDialogWasShownAndClosedByUser &&
                CanShowMutedMessage && (
                    <MessageMuted close={setCanShowMutedMessage} />
                )}
        </ContentViewCont>
    );
});
