import React from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";
// ====================================================================================
// styled components
import {
    VideoWrapper,
    ContentViewCont,
    // ScrollWrap,
} from "./ContentView_SC.jsx";
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
import VideoControlsStore from "../../store/VideoControlsStore.js";
import PagesStore from "../../store/PagesStore.js";
import LandingsStore from "../../store/LandingsStore.js";
import ViewPortSizeStore from "../../store/ViewPortSizeStore.js";

// ====================================================================================
// ====================================================================================
// ====================================================================================
import CardComponent from "../_3_chunks/Card__Cmp.jsx";
// ====================================================================================
// *********************************************

// *********************************************
// *********************************************

export default observer(function ContentView() {
    const {
        videoUrl,
        globalPlayEnabled,
        setShowGlobalCovers,
    } = VideoControlsStore;
    const { pages, shownIndex } = PagesStore;
    const { selectedLending } = LandingsStore;
    const VideoComponent = (
        <VideoWrapper>
            <ReactPlayer
                width="100%"
                height="120%"
                url={videoUrl}
                controls={false}
                muted={false}
                loop={true}
                playsinline={true}
                playing={globalPlayEnabled}
                onPlay={() => setShowGlobalCovers(false)}
                onPause={() => setShowGlobalCovers(true)}
                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            modestbranding: 1,
                            showinfo: 0,
                            origin: "https://candylanding.netlify.app/",
                            enablejsapi: 1,
                        },
                    },
                }}
            />
        </VideoWrapper>
    );
    let newarray = [];
    if (selectedLending) {
        newarray = selectedLending.pages.sort((a, b) => a.order > b.order);
        // console.log(newarray);
    }

    const handleFetchMoreData = () => {};
    // console.log(pages);
    return (
        <ContentViewCont id="scrollableDiv">
            {/* <ScrollWrap > */}
            <div style={{ overflow: "hidden" }}>
                <InfiniteScroll
                    dataLength={newarray.length}
                    next={handleFetchMoreData}
                    style={{
                        position: "relative",
                        // minHeight: "100%",
                        height: ViewPortSizeStore.sizeOfContentView.height,
                        // maxHeight: "100%",
                        // minWidth: "100%",
                        width: ViewPortSizeStore.sizeOfContentView.width,
                        // maxWidth: "100%",
                        overflow: "scroll",
                        overflowX: "hidden",
                        scrollSnapType: "y mandatory",
                        scrollbarWidth: "thin",
                        msOverflowStyle: "none",

                        // -ms-overflow-style: none,
                        // &::-webkit-scrollbar {
                        //     display: none,
                        // }
                    }} //To put endMessage and loader to the top.
                    // inverse={true} //
                    hasMore={true}
                    loader={<h4>Loading...</h4>}
                    // scrollableTarget="scrollableDiv"
                >
                    {pages &&
                        newarray &&
                        newarray.map((__page, index) => {
                            console.log(__page._id);
                            const page = pages.find(
                                (item) => item._id === __page._id
                            );
                            if (page)
                                return (
                                    <CardComponent
                                        key={index}
                                        myIndex={index}
                                        page={page}
                                        likes={1}
                                        shares={2}
                                        shownIndex={shownIndex}
                                    />
                                );
                            else return null;
                        })}
                </InfiniteScroll>
            </div>
            {/* </ScrollWrap> */}
            {SystemStore.isIOS && VideoComponent}
        </ContentViewCont>
    );
});
