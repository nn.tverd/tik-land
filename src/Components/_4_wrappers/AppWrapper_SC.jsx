import styled from "styled-components";
// *********************************************
// *********************************************
export const ContentViewCont = styled.div``;

export const ContentContainer = styled.div`
    display: flex;
    // place-items: center;
    position: relative;
    top: 0;
    left: 0;

    background-color: black;
    height: 100%;   
    max-height: 100%;

    max-width: 100%;
    width: 100%;
    min-width: 100%;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
        display: none;
    }
`;

export const NavContainer = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    min-height: 60px;
    height: calc(100% - ${(props) => props.viewPortHeight}px);
    // background-color: orange;
    background-color: rgba(0, 0, 0, 0);
    // border-top: 1px solid gray;
    scrollbar-width: none;

    max-width: 100%;
    width: 100%;
    min-width: 100%;
    // overflow: hidden;
    // scroll-snap-type: y mandatory;
    display: grid;
    place-items: center;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
        display: none;
    }
    z-index: 0;
`;
