import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
// ====================================================================================
// import {} from "./ContentView_SC.jsx";
// ====================================================================================
// import { setAllOnPlaceFalse } from "../../redux/ducks/system.js";
import DebuggerView from "./DebuggerView.jsx";
// ====================================================================================
import PagesStore from "../../store/PagesStore.js";
import LandingsStore from "../../store/LandingsStore.js";
// ====================================================================================
// import ContentView from "./ContentView_Cnt.js";
import MainAction from "../_3_chunks/MainAction__Cmp.jsx";
import ContentView_IOS from "./ContentView_IOS.jsx";
// import ContentView_NonIOS from "./ContentView_NonIOS.jsx";

// ====================================================================================
import { ContentContainer, NavContainer } from "./AppWrapper_SC.jsx";
// ====================================================================================
import ViewPortSizeStore from "../../store/ViewPortSizeStore.js";
import { useParams } from "react-router-dom";
// import SystemStore from "../../store/SystemStore.js";

// ====================================================================================
// *********************************************

// *********************************************
// *********************************************

export default observer(function ContentViewContainer(props) {
    // const { setPages, numOfPagesWereLoad } = PagesStore;
    // const { landingId } = useParams();
    // const { getLandingById } = LandingsStore;
    
    return (
        <ContentContainer>
            {/* {isIOS ? <ContentView_IOS /> : <ContentView_NonIOS />} */}
            <ContentView_IOS />

            <DebuggerView />
            {/* <NavContainer
                viewPortHeight={ViewPortSizeStore.sizeOfContentView.height}
            >
                <MainAction />
            </NavContainer> */}
        </ContentContainer>
    );
});
