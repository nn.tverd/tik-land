import React, { useState, useEffect } from 'react';

// Наш хук
export default function useDebounce(inputValue, value1, delay1, value2, delay2) {
  // Состояние и сеттер для отложенного значения
  const [debouncedValue, setDebouncedValue] = useState(inputValue);

  let delay = 0;
  if(value1 === inputValue){
    delay = delay1;
  }
  if(value2 === inputValue){
    delay = delay2;
  }

  useEffect(
    () => {
      // Выставить debouncedValue равным inputValue (переданное значение) 
      // после заданной задержки
      const handler = setTimeout(() => {
        setDebouncedValue(inputValue);
      }, delay);

      // Вернуть функцию очистки, которая будет вызываться каждый раз, когда ...
      // ... useEffect вызван снова. useEffect будет вызван снова, только если ...
      // ... inputValue будет изменено (смотри ниже массив зависимостей).
      // Так мы избегаем изменений debouncedValue, если значение inputValue ...
      // ... поменялось в рамках интервала задержки.
      // Таймаут очищается и стартует снова.
      // Что бы сложить это воедино: если пользователь печатает что-то внутри ...
      // ... нашего приложения в поле поиска, мы не хотим, чтобы debouncedValue...
      // ... не менялось до тех пор, пока он не прекратит печатать дольше, чем 500ms.
      return () => {
        clearTimeout(handler);
      };
    },
    // Вызывается снова, только если значение изменится
    // мы так же можем добавить переменную "delay" в массива зависимостей ...
    // ... если вы собираетесь менять ее динамически.
    [inputValue]
  );

  return debouncedValue;
}