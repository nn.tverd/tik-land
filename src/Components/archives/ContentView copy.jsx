import React, { useState } from "react";
import styled from "styled-components";
import CardComponent from "../_3_chunks/Card__Cmp.jsx";

const ContentViewCont = styled.div`
    position: relative;

    min-height: 100%;
    height: 100%;

    max-height: 100%;
    min-width: 100%;
    width: 100%;
    max-width: 100%;
    overflow: hidden;
    // background-color: tomato;
    scrollbar-width: none;
    border-radius: 10px;

    display: grid;
    place-items: center;
`;
const ScrollWrap = styled.div`
    position: relative;
    // background-color: yellow;

    min-height: 100%;
    height: 100%;
    max-height: 100%;

    min-width: 100%;
    width: 100%;
    max-width: 100%;
    overflow: scroll;
    scroll-snap-type: y mandatory;
    scrollbar-width: none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
        display: none;
    }
`;

export default function ContentView(props) {
    const [firstVideoPlayForce, setFirstVideoPlayForce] = useState(false);
    const [globalPlaying, setglobalPlaying] = useState(false);
    const playingStaff = {
        firstVideoPlayForce,
        setFirstVideoPlayForce,
        globalPlaying,
        setglobalPlaying,
    };
    const [pauseAllVideos, setPauseAllVideos] = useState(0)
    const { pages, likes, shares, shownIndex, setShownIndex, navOverlap } = props;
    return (
        <ContentViewCont>
            <ScrollWrap>
                {pages &&
                    pages.map((page, index) => {
                        if(index> 3) return null;
                        const l = likes + Math.round(Math.random() * likes);
                        return (
                            <CardComponent
                                key={index}
                                myIndex={index}
                                page={page}
                                likes={l}
                                shares={shares}
                                shownIndex={shownIndex}
                                setShownIndex={setShownIndex}
                                navOverlap={navOverlap}
                                playingStaff={playingStaff}
                                pauseAllVideos={pauseAllVideos}
                                setPauseAllVideos={setPauseAllVideos}
                                
                            />
                        );
                    })}
            </ScrollWrap>
        </ContentViewCont>
    );
}
