import React from "react";
import styled from "styled-components";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import MenuBookIcon from "@material-ui/icons/MenuBook";
// import PlaylistAddCheckIcon from "@material-ui/icons/PlaylistAddCheck";
// import { useSelector } from "react-redux";
import "../css/animations.css";

const MainButtons = styled.div`
    position: absolute;
    height: 100%;
    top: 50%;
    transform: translateY(-50%);
    border-radius: 20px;
    margin-top: 10px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
`;
const CentralWrapper = styled.div`
    position: relative;
    display: block;
    margin
    place-items: center;
    border: 1px solid white;
`;
const CentralButton = styled.a`
    position: relative;
    top: -60px;
    width: 100px;
    height: 100px;
    margin-top: 0px;
    margin-bottom: 0px;
    margin: 0 aut0;
    // border-radius: 50%;
    // border: 1px solid white;
    text-decoration: none;
    text-transform: uppercase;
    display: grid;
    place-items: center;

    color: white;
    font-size: 17px;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
    // &::before{
    //     content:"";
    //     border: 2px solid white;
    //     border-radius: 100%;
    //     animation: 1s linear infinite alternate abberation, 4s linear infinite spin;
    //     // padding-top:30%;
    //     position: relative;
    //     pointer-events:none;

    // }
`;
const CentralIcon = styled.div`
    position: relative;
    width: 30px;
    height: 20px;
    margin-top: 0px;
    margin-bottom: 0px;

    border-radius: 3px;
    text-decoration: none;
    text-transform: uppercase;
    padding: 1em 1em;
    // display: flex;
    justify-content: center;
    /* align-items: center; */
    // background-color: ${(props) => props.bgcolor};
    // border: ${(props) => props.borderStyle};

    color: white;
    font-size: 5px;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
`;
const CentralButtonIconAndTextWrap = styled.div`
    position: absolute;
    height: 100%;
    width: 100%;
    display: block;
    place-items: center;
`;
const SideButton = styled.button`
    position: relative;
    margin: 0 aut0;
    // width: 40px;
    height: 40px;
    margin: 0;
    margin-left: 0px;
    margin-right: 0px;
    background-color: rgba(0, 0, 0, 0);
    border: 0px;
    padding: 0;

    text-decoration: none;
    text-transform: uppercase;
    justify-content: center;
    color: white;
    font-size: 7px;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
    outline: none !important;
`;
const SideIcon = styled(CentralIcon)`
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid white;
    color: white;
`;

const ButtonsCell = styled.div`
    // position: absolute;
    width: 17%;
    display: flex;
    justify-content: center;
    height: 100%;
`;
const ButtonsCellCentral = styled(ButtonsCell)`
    width: 32%;
`;

export default function MainAction(props) {
    const { pages } = props;
    // const shownIndex = useSelector(
    //     (state) => state.videoControls.onPlace__Index
    // );
    const page = pages[shownIndex];
    const Icon = page.iconForSidebar;
    let ShowBlock = null;

    if (page.showSliderHint && page.buttonType === 0) {
        ShowBlock = <div className="slider__down"></div>;
    }
    // if (false) {
    if (page.buttonType > 0) {
        if (page.buttonType === 1) {
            ShowBlock = (
                <CentralWrapper>
                    <CentralButton
                        // className={superClassForMainButton}
                        href={page.buttonUrl}
                        target="_blank"
                        rel="noreferrer"
                        // isVis={page.buttonType}
                    >
                        <CentralIcon
                            bgcolor={"rgba(0,0,0,0)"}
                            borderStyle={"2px solid white"}
                        >
                            <ShoppingCartIcon fontSize="small" />
                            <div>{page.buttonText}</div>
                        </CentralIcon>
                    </CentralButton>
                    <div className="button-animation"></div>
                </CentralWrapper>
            );
        }
        if (page.buttonType === 2) {
            ShowBlock = (
                <CentralWrapper>
                    <CentralButton
                        // className={superClassForMainButton}
                        href={page.buttonUrl}
                        target="_blank"
                        rel="noreferrer"
                        isVis={page.buttonType}
                    >
                        <CentralIcon bgcolor={"tomato"} borderStyle={"0px"}>
                            <ShoppingCartIcon fontSize="small" />
                            <div>{page.buttonText}</div>
                        </CentralIcon>
                    </CentralButton>
                    <div className="button-animation"></div>
                </CentralWrapper>
            );
        }
    }
    if (page.buttonType > -1)
        return (
            <MainButtons>
                <ButtonsCell></ButtonsCell>
                <ButtonsCell>
                    {page.showMainActionSideButtons && (
                        <SideButton
                            onClick={(e) => {
                                alert(page.lawData);
                            }}
                        >
                            <SideIcon>
                                <MenuBookIcon fontSize="small" />
                            </SideIcon>
                            <div>Юр. инфо</div>
                        </SideButton>
                    )}
                </ButtonsCell>
                <ButtonsCellCentral>
                    {/* <ShowBlock ></ShowBlock> */}
                    {ShowBlock}
                </ButtonsCellCentral>

                <ButtonsCell>
                    {page.showMainActionSideButtons && (
                        <SideButton
                            onClick={(e) => {
                                window.open(page.linkFromSideBar, "_blank");
                            }}
                        >
                            <SideIcon>
                                <Icon fontSize="small" />
                            </SideIcon>
                            <div>Соглашение</div>
                        </SideButton>
                    )}
                </ButtonsCell>
                <ButtonsCell></ButtonsCell>
            </MainButtons>
        );
    else return null;
}
