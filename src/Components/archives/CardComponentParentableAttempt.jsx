import React, { useRef, useState, useEffect } from "react";
import { Waypoint } from "react-waypoint";
import styled from "styled-components";
// ====================================================================================
// import "./Video/Video.css";
// ====================================================================================
import Footer from "../_2_molecules/Footer.jsx";
import Sidebar from "../Sidebar.jsx";
// ====================================================================================

// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import TopAlert from "../_2_molecules/TopAlert";
// import MainAction from "./MainAction";
import HeaderComponent from "../_3_chunks/Header__Cmp";
// import Video from "./Video.jsx";
// import VideoAdvanced from "./VideoAdvanced.jsx";
import VideoHlsOnePlayer from "../_3_chunks/VideoHlsOnePlayer__Cmp.jsx";
// import VideoHls from "./VideoHls.jsx";
// import useDebounce from "./Debounce.js";

const CardComp = styled.div`
    position: relative;
    background-color: none;
    width: 100%;
    height: 100%;
    scroll-snap-align: start;
    scroll-snap-stop: always;
`;
const ParentWrapper = styled.div`
    position: absolute;
    background-color: none;
    width: 100%;
    height: 100%;
    display: grid;
    place-items: center;
`;

export default function CardComponent(props) {
    const {
        likes,
        shares,
        page,
        shownIndex,
        setShownIndex,
        myIndex,
        navOverlap,
        playingStaff,
        pauseAllVideos,
        setPauseAllVideos,
    } = props;
    const {
        Reparentable,
        sendReparentableChild,
        pages,
        VideoComponent,
    } = playingStaff;
    const [playing, setPlaying] = useState(false);
    const [log, setLog22] = useState("123");

    const mediaRef = useRef(null);

    useEffect(() => {
        if (shownIndex !== myIndex) setPlaying(false);

        setShownIndex(myIndex);
    }, [pauseAllVideos]);

    // const handleWaypointLeave = () => {
    //     setPlaying(false);
    // };
    const handleEnterViewPort = () => {
        playingStaff.setPlayingUrl(page.videoUrl);

        setShownIndex(myIndex);
        setPauseAllVideos(pauseAllVideos + 1);
        if (playingStaff.globalPlaying) {
            setPlaying(true);
        }

        if (page.type === "video") {
            sendReparentableChild(
                `parent${pages[shownIndex]._id}`,
                `parent${page._id}`,
                0,
                0
            );
        }
    };

    const handleLeaveViewPort = () => {
        setPlaying(false);
    };
    const cardRef = useRef(null);
    let intervals = setInterval(() => {
        if (cardRef.current && myIndex === shownIndex) {
            const rect = cardRef.current.getBoundingClientRect();
        }
    }, 2500);

    return (
        <Waypoint
            topOffset="29%"
            bottomOffset="69%"
            onEnter={handleEnterViewPort}
            onLeave={handleLeaveViewPort}
        >
            <CardComp
                ref={cardRef}
                onTouchStart={() => {
                    playingStaff.setFseShowsCover(true);
                }}
                onTouchMove={(e) => {
                    const rect = cardRef.current.getBoundingClientRect();
                    playingStaff.setYVideoPosition(rect.y);
                }}
                onTouchEnd={() => {
                    playingStaff.setFseShowsCover(false);
                    playingStaff.setYVideoPosition(0);
                }}
                onClick={(e) => {
                    playingStaff.setglobalPlaying(!playingStaff.globalPlaying);
                }}
            >
                {page.type === "img" && (
                    <HeaderComponent
                        page={page}
                        playing={playing}
                        // mediaRef={mediaRef}
                        setPlaying={setPlaying}
                    />
                )}
                {page.type === "video" && (
                    <ParentWrapper className=" lookhere ">
                        <VideoHlsOnePlayer
                            page={page}
                            playing={playing}
                            mediaRef={mediaRef}
                            setPlaying={setPlaying}
                            playingStaff={playingStaff}
                            log={log}
                            setLog22={setLog22}
                        />
                        <Reparentable id={`parent${page._id}`}>
                            {shownIndex === myIndex ? [VideoComponent] : []}
                        </Reparentable>
                    </ParentWrapper>
                )}

                <TopAlert page={page} playingStaff={playingStaff} />
                <Footer page={page} navOverlap={navOverlap} />
                <Sidebar likes={likes} shares={shares} page={page} />
            </CardComp>
        </Waypoint>
    );
}
