import React from "react";
// ====================================================================================
import "./css/common.css";
// ====================================================================================
// ====================================================================================

import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import styled from "styled-components";
// import ReactHLS from 'react-hls';
import ReactPlayer from "react-player/lazy";
// In your render function
const VideoWrap = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    display: grid;
    place-items: center;
`;

// const VideoComp = styled.video`
//     position: absolute;
//     // object-fit: fill;
//     width: 101%;
//     height: auto;
//     top: 50%;
//     transform: translateY(-50%);
//     z-index: 1;
// `;

const PlayButton = styled.div`
    position: absolute;
    z-index: 500;
    /* width: auto; */
    color: white;
    display: flex;
    height: auto;
    // top: 50%;
    // left: 50%;
    // transform: translateX(-50%);
    // transform: translateY(-50%);
`;
const VideoOverlay = styled.div`
    z-index: 2;
    position: absolute;
    width: 100%;
    height: 100%;
    // background-color: none;
    background-color: rgba(0, 0, 0, 0.95);

    display: grid;
    place-items: center;
`;

export default function Video(props) {
    const {
        page,
        playing,
        setPlaying,
        mediaRef,
        playingStaff,
        log,
        setLog22,
    } = props;

    const showToPage = {
        playing,
        setPlaying,
        mediaRef,
        playingStaff,
    };
    // const url = page.videoUrl;
    // const url = "https://www.youtube.com/watch?v=-O0An_gh6wU";
    const url = "http://ccut.tk/land_trokot-alcantara/fragmented.mp4";
    let showCower = null;
    if (
        url.includes("://") &&
        url.includes("youtube.com") &&
        !playingStaff.firstVideoPlayForce
    ) {
        showCower = url.split("?v=")[1].split("&")[0];
    }

    const handleVideoPress = () => {
        let logstr = log;
        logstr += " = = = = =  " + "  useEffect  ";

        logstr += " playing = " + playing;
        logstr += " globalPlaying = " + playingStaff.globalPlaying;
        logstr += " |||||||||  ";
        setLog22(logstr);
        //log + " const handleVideoPress = () => {"
        setPlaying(!playing);
        playingStaff.setglobalPlaying(!playingStaff.globalPlaying);
        playingStaff.setFirstVideoPlayForce(true);
    };
    // if (page.type === "video" && playingStaff.firstVideoPlayForce) {
    //     playingStaff.setFirstVideoPlayForce(false);
    //     setPlaying();
    // }
    const imgStyle = {
        position: "absolute",
        height: "120%",
        width: "auto",
    };
    return (
        <VideoWrap>
            {/* <ReactHLS url={"http://ccut.tk/land_trokot-alcantara/fragmented.mp4"} /> */}
            <VideoOverlay onClick={handleVideoPress}>
                {showCower && (
                    <img
                        style={imgStyle}
                        src={`http://img.youtube.com/vi/${showCower}/0.jpg`}
                        alt="1"
                    />
                )}
                <div>props: {JSON.stringify(showToPage)}</div>
                <div>log:{log}</div>
            </VideoOverlay>
            <ReactPlayer
                width="100%"
                height="150%"
                url={url}
                controls={false}
                loop={true}
                playsinline={true}
                playing={playing}
                // playing={true}
                // autoPlay={true}
                // muted={true}

                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            // autoplay: 1,
                        },
                    },
                }}
            />
            {!playing && (
                <PlayButton onClick={handleVideoPress}>
                    <PlayArrowIcon
                        fontSize="large"
                        className="svg_icons_play"
                    />
                </PlayButton>
            )}
        </VideoWrap>
    );
}
