import React, { useState } from "react";

export default function AddInfoButton(props) {
    const { Icon, messageCount, link } = props;
    const [showAddBlock, setShowAddBlock] = useState(false);
    return (
        <div className="addInfoContainer">
            <Icon fontSize="large" onClick={(e)=>{setShowAddBlock(true)}}/>
            <p className="sideBarNotes">{messageCount}</p>
            {showAddBlock && <div className="addInfoBlock">12312312</div>}
        </div>
    );
}
