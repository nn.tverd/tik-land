import React, { useEffect, useRef, useState } from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";
// import VideoControlsStore from "../../store/VideoControlsStore";

export default observer(function Player({
    videoUrl,
    paused,
    setCanShowMutedMessage,
    setShowGlobalCovers,
    muted,
    checkUnmuteObj,
    page,
}) {
    // const ref = useRef(null);
    const [playing, setPlaying] = useState(false);
    // const [lock, setLock] = useState(true);
    const [enablePause, setEnablePause] = useState(false);
    const [enablePlay, setEnablePlay] = useState(true);

    useEffect(() => {
        if (paused && enablePause) {
            setEnablePause(false);
            setPlaying(false);
        }
        if (!paused && enablePlay) {
            setEnablePlay(false);
            setPlaying(true);
        }
    }, [paused, enablePause, enablePlay]);

    const handleUnmuteOnPlay = () => {
        const {
            controlCanBeUnmutedTimer,
            canBeUnmuted,
            setCanBeUnmuted,
            setControlCanBeUnmutedTimer,
            setMuted,
        } = checkUnmuteObj;
        if (canBeUnmuted === -1) {
            clearTimeout(controlCanBeUnmutedTimer);
            setMuted(false);
            setControlCanBeUnmutedTimer(
                setTimeout(() => {
                    setCanBeUnmuted(1);
                }, 500)
            );
        }
    };

    return (
        <ReactPlayer
            width="100%"
            height="140%"
            url={videoUrl}
            // url={"https://drive.google.com/uc?export=download&id=1jpqIH7bTcobbBjxlITMvtwqqwuoD3YfM"}
            controls={false}
            loop={true}
            playsinline={true}
            // playing={globalPlayEnabled && !Paused}
            playing={playing}
            // muted={SystemStore.shouldBeMuted}
            muted={checkUnmuteObj.muted}
            onPause={() => {
                const {
                    controlCanBeUnmutedTimer,
                    canBeUnmuted,
                    setCanBeUnmuted,
                    setMuted,
                } = checkUnmuteObj;
                // setLock(false);
                setEnablePlay(true);
                setShowGlobalCovers(true);
                if (canBeUnmuted === -1) {
                    clearTimeout(controlCanBeUnmutedTimer);
                    setMuted(true);
                    setPlaying(false);
                    setTimeout(() => {
                        setPlaying(true);
                    }, 2000);
                    // setPlaying(false);
                    setCanBeUnmuted(0);
                    setCanShowMutedMessage(true);
                    // alert("can not be unmuted");
                }
                // PagesStore.setAllPagesPaused(shownIndex);
            }}
            onPlay={() => {
                // VideoControlsStore.setLockChangeVideo(false);
                // setLock(false);
                handleUnmuteOnPlay();
                setEnablePause(true);
                setShowGlobalCovers(false);
                // PagesStore.setAllPagesPaused(page._id);
                // setTimeout(() => {
                    // setCanShowMutedMessage(true);
                // }, 3000);
            }}
            config={{
                youtube: {
                    playerVars: {
                        rel: 0,
                        iv_load_policy: 3,
                        modestbranding: 1,
                        showinfo: 0,
                        origin: window.location.origin,
                        enablejsapi: 1,
                        // autoplay: true,
                        // start: 50
                    },
                },
            }}
        />
    );
});
