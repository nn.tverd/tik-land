import React, { useState } from "react";
import { observer } from "mobx-react-lite";
// ====================================================================================
import { Button, IconButton } from "@material-ui/core";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";
import CancelIcon from "@material-ui/icons/Cancel";
// ====================================================================================
import {
    VideoWrapper,
    // ContentViewCont,
    // ScrollWrap,
} from "../_4_wrappers/ContentView_SC.jsx";
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
// ====================================================================================
export default observer(function MessageMuted({ close }) {
    const [step, setStep] = useState(0);

    const [rus, setRus] = useState(false);
    const lang = {
        en: {
            header:
                "Facebook is limiting you from whatching video with sound on this web page",
            useDefault: "Use Chrome, Mozilla, Opera, or Safari browser.",
            unmute1: "Or try to unmute video by clicking 'Unmute' red button",
            solutionButton: "Solution",
            exitButton: "Got it",
            advancedShort: "Playing sound on this web page is prohibited.",
            solutionHint:
                "Click ... on the right top corner and select 'Open with [Chrome | Safari | Opera | Mozilla]'",
        },
        ru: {
            header:
                "Facebook ограничивает вам просмотр видео со звуком на нашем сайте.",
            useDefault: "Используйте (Chrome, Safari, Opera или Mozilla).",
            unmute1:
                "Или попробуйте включить звук нажатием на красную кнопку слева.",
            solutionButton: "Решение",
            exitButton: "Понятно",
            advancedShort: "Данный браузер не воспроизводит звук.",
            solutionHint:
                "Нажимай ... в правом верхнем углу, выбирай 'Открыть в [Chrome | Safari | Opera | Mozilla]'",
        },
    };
    const choosenLang = rus ? lang.ru : lang.en;

    // setStep(1);
    return (
        <div
            style={{
                width: "100%",
                position: "fixed",
                zIndex: 100,
                backgroundColor: "rgba(255,255,255,.9)",
                top: 0,
                left: 0,
            }}
        >
            <div
                style={{
                    display: "grid",
                    placeItems: "center",
                    gridTemplateColumns: "1fr 3fr 3fr",

                    width: "100%",
                }}
            >
                <div>
                    <IconButton
                        style={{
                            color: "tomato",
                        }}
                    >
                        <VolumeOffIcon size="large" />
                    </IconButton>
                    <Button
                        style={{
                            fontSize: "0.6rem",
                        }}
                        onClick={() => {
                            setRus(!rus);
                        }}
                    >
                        {!rus ? "ru" : "en"}
                    </Button>
                </div>
                <div style={{ color: "black" }}>
                    {choosenLang.advancedShort}
                </div>
                <div style={{ color: "black", fontSize: "1.8rem" }}>
                    <Button
                        variant="contained"
                        color="secondary"
                        style={{ fontSize: ".6rem" }}
                        onClick={() => {
                            setStep(1);
                        }}
                    >
                        {choosenLang.solutionButton}
                    </Button>
                    <Button
                        color="primary"
                        variant="outlined"
                        style={{ fontSize: ".6rem" }}
                        onClick={() => {
                            close();
                        }}
                    >
                        {choosenLang.exitButton}
                    </Button>
                </div>
            </div>
            {step !== 0 && (
                <div
                    style={{
                        color: "black",
                        padding: 15,
                    }}
                >
                    <h3>{choosenLang.useDefault}</h3>
                    <div>
                        <img
                            src={"away.gif"}
                            width="100%"
                            height="auto"
                            alt="loading..."
                        />
                    </div>
                    <div>
                        <p>{choosenLang.solutionHint}</p>
                    </div>
                    <div
                        style={{
                            display: "grid",
                            placeItems: "center",
                            width: "100%",
                        }}
                    >
                        <IconButton onClick={() => close()}>
                            <CancelIcon />
                        </IconButton>
                    </div>
                </div>
            )}
        </div>
    );
});
