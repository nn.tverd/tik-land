import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import Avatar from "react-avatar";
import "../css/common.css";
import FavoriteRoundedIcon from "@material-ui/icons/FavoriteRounded";
import ReplyRoundedIcon from "@material-ui/icons/ReplyRounded";
import DirectionsRoundedIcon from "@material-ui/icons/DirectionsRounded";
// import VerifiedUserRoundedIcon from "@material-ui/icons/VerifiedUserRounded";
import StarsRoundedIcon from "@material-ui/icons/StarsRounded";
// import FavoriteIcon from "@material-ui/icons/Favorite";
// import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
// import MessageIcon from "@material-ui/icons/Message";
import CancelRoundedIcon from "@material-ui/icons/CancelRounded";
// import ShareIcon from "@material-ui/icons/Share";
// import conf from "../../data/cf.js";

import styled from "styled-components";
import ModalShare from "./ModalShare";

const SidebarWrap = styled.div`
    // z-index: 15;
    position: absolute;
    bottom: 180px;
    right: 15px;
    color: white;
    margin: 0px;
    pointer-events: all;

    padding: 0px;
`;
const SidebarIconWrap = styled.div`
    padding: 5px;
    text-align: center;
    margin: 0px;
    padding: 0px;
    margin-bottom: 20px;
    z-index: 19;
    position: relative;
`;
const AvatarHolder = styled.div`
    background-color: white;
    width: 55px;
    height: 55px;
    border-radius: 50%;
    display: grid;
    place-items: center;
`;
// const VerifySignHolderOffset = styled.div``;
const VerifySignHolder = styled.div`
    background-color: white;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    display: grid;
    border: 0px;
    padding: 0;
    place-items: center;
    position: absolute;
    top: 45px;
    left: 50%;
    // transform: translateY(-50%);
    transform: translateX(-50%);
    z-index: 1;
    color: ${(props) => (props.fColor ? props.fColor : "tomato")};
`;
const VerifyIconWrapper = styled.div`
    position: absolute;
    top: 50%;
    display: block;
    z-index: 1;
    transform: scale(0.8) translateY(-56%);
    padding: 0;
    margin: 0;
`;

// import ShareButton from "./ShareButton.jsx";
export default observer(function Sidebar(props) {
    // const { loading, isSupported, share } = useWebShare();
    const {
        page,
        isVerified,
        avatarUrl,
        shareUrl,
        shareTitle,
        updateLikes,
        updateShares,
    } = props;
    // console.log("\n\n\n\n\n AvatarURl", avatarUrl);
    const [showModal, setShowModal] = useState(false);
    const { likes, shares } = page;
    // const AddBlockComponent = page.sideBarComponent;
    const [liked, setLiked] = useState(false);
    const [shared, setShared] = useState(false);
    const [visited, setVisited] = useState(false);

    // console.log("updateLikes,  updateShares,", updateLikes, updateShares);

    const handleShareButton = () => {
        // Check if navigator.share is supported by the browser
        if (!shareTitle || !shareUrl) {
            return alert(
                "Host of the landing does not provide info for sharing."
            );
        }
        if (navigator.share) {
            navigator
                .share({
                    url: shareUrl,
                    title: shareTitle,
                })
                .then(() => {
                    // console.log("Sharing successfull");
                })
                .catch(() => {
                    // console.log("Sharing failed");
                });
        } else {
            // alert("Sorry! Your browser does not support Web Share API");
            setShowModal(true);
        }
    };
    // const MessageIcon2 = page.sideBarIcon ? page.sideBarIcon : MessageIcon;
    const messageCount = page.sideBarTitle ? page.sideBarTitle : 16;

    const handleGoAway = (e) => {
        // sideBarMessage
        if (window.confirm(page.sideBarMessage)) {
            // Save it!
            window.open(page.sideBarUrl, "_blank");
        } else {
            // Do nothing!
            // console.log("User Cancel");
        }
    };

    const convertThousands = (number) => {
        const num = Number(number);
        const isnan = isNaN(num);
        if (isnan) {
            return number;
        }

        if (number < 1000) return number;
        if (number < 1000000) return `${Math.round(number / 100) / 10.0}k`;
        return `${Math.round(number / 100000) / 10.0}M`;
    };
    const style = {
        margin: 0,
    };

    const buttonBlinkingClass = page.sideBarBlick
        ? "blicking-button-side-bar"
        : "";
    return (
        <SidebarWrap>
            <SidebarIconWrap>
                <AvatarHolder>
                    <VerifySignHolder fColor={isVerified ? "green" : "tomato"}>
                        <VerifyIconWrapper>
                            {isVerified ? (
                                <StarsRoundedIcon />
                            ) : (
                                <CancelRoundedIcon />
                            )}
                        </VerifyIconWrapper>
                    </VerifySignHolder>
                    <Avatar size="50" round={true} src={avatarUrl} />
                </AvatarHolder>
            </SidebarIconWrap>
            {page.showLikes && (
                <SidebarIconWrap>
                    {liked ? (
                        <span style={{ color: "tomato" }}>
                            <FavoriteRoundedIcon
                                fontSize="large"
                                onClick={(e) => {
                                    e.stopPropagation();
                                    setLiked(false);
                                }}
                            />
                        </span>
                    ) : (
                        <span>
                            <FavoriteRoundedIcon
                                fontSize="large"
                                onClick={(e) => {
                                    e.stopPropagation();
                                    setLiked(true);
                                    updateLikes(page.landing);
                                }}
                            />
                        </span>
                    )}
                    <p className="sideBarNotes" style={style}>
                        {liked
                            ? convertThousands(likes + 1)
                            : convertThousands(likes)}
                    </p>
                </SidebarIconWrap>
            )}

            {page.showSidebarButton && (
                <SidebarIconWrap>
                    <div className={buttonBlinkingClass}>
                        <DirectionsRoundedIcon
                            fontSize="large"
                            onClick={(e) => {
                                e.stopPropagation();
                                handleGoAway(e);
                                setVisited(true);
                            }}
                        />
                    </div>
                    <p className="sideBarNotes" style={style}>
                        {visited
                            ? convertThousands(messageCount)
                            : convertThousands(messageCount)}
                    </p>
                    {/* <AddBlockComponent Icon={MessageIcon2} messageCount={messageCount} link="google.com"/> */}
                </SidebarIconWrap>
            )}

            {page.showShares && (
                <SidebarIconWrap>
                    {/* <ShareButton/> */}
                    {/* <ShareButton config={config} foo="bar" />; */}
                    <ReplyRoundedIcon
                        fontSize="large"
                        onClick={(e) => {
                            e.stopPropagation();
                            setShared(true);
                            handleShareButton();
                            updateShares(page.landing);
                        }}
                    />
                    <p className="sideBarNotes" style={style}>
                        {shared
                            ? convertThousands(shares + 1)
                            : convertThousands(shares)}
                    </p>
                    <ModalShare show={showModal} setShow={setShowModal} />
                </SidebarIconWrap>
            )}
        </SidebarWrap>
    );
});
