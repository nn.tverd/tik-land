import React from "react";
import styled from "styled-components";
import { observer } from "mobx-react-lite";

// import VideoControlsStore from "../../store/VideoControlsStore.js";
// import PagesStore from "../../store/PagesStore.js";

// import "./common.css";
import MusicNoteIcon from "@material-ui/icons/MusicNote";
import Ticker from "react-ticker";
import "../css/animations.css";

const FooterWrap = styled.div`
    z-index: 15;
    position: absolute;
    color: white;
    bottom: 60px;
    margin-left: 10%;
    width: 80%;

    // display: r;
`;
const FooterTextWrap = styled.div`
    flex: 1;
`;
const FooterAccName = styled.h3`
    padding: 2px;
    margin-bottom: 0px;
    user-select: none;
    -webkit-user-select: none;
`;
const FooterDescr = styled.p`
    margin-top: 0px;
    padding: 2px;
    width: 100%;
    font-size: 15px;
    user-select: none;
    -webkit-user-select: none;
`;
const FooterTickerWrap = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const FooterTickerCell = styled.div`
    display: flex;
    align-items: center;

    width: ${(props) => props.customWidthPercent}%;
`;

export default observer(function Footer(props) {
    const { page } = props;
    const FooterIcon = page.songIcon
        ? page.songIcon === "standart"
            ? MusicNoteIcon
            : page.songIcon
        : MusicNoteIcon;

    return (
        <FooterWrap>
            <FooterTextWrap>
                <FooterAccName className="pNoneSelect">
                    {page.accname}
                </FooterAccName>
                <FooterDescr>{page.description}</FooterDescr>
                <FooterTickerWrap>
                    <FooterTickerCell customWidthPercent={10}>
                        <FooterIcon
                            fontSize="small"
                            className="videoFooter__icon"
                        />
                    </FooterTickerCell>
                    <FooterTickerCell customWidthPercent={90}>
                        <div className="videoFooter__ticker">
                            <Ticker mode="await">
                                {({ index }) => (
                                    <>
                                        <p className="pNoneSelect">
                                            {page.songTitle}
                                        </p>
                                    </>
                                )}
                            </Ticker>
                        </div>
                    </FooterTickerCell>
                </FooterTickerWrap>
            </FooterTextWrap>
        </FooterWrap>
    );
});
