import React from "react";
import styled from "styled-components";
import { observer } from "mobx-react-lite";
import CancelIcon from "@material-ui/icons/Cancel";
import LandingsStore from "../../store/LandingsStore.js";

import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    LinkedinIcon,
    LinkedinShareButton,
    MailruIcon,
    MailruShareButton,
    OKIcon,
    OKShareButton,
    RedditIcon,
    RedditShareButton,
    TelegramIcon,
    TelegramShareButton,
    TumblrIcon,
    TumblrShareButton,
    TwitterIcon,
    TwitterShareButton,
    ViberIcon,
    ViberShareButton,
    VKIcon,
    VKShareButton,
    WhatsappIcon,
    WhatsappShareButton,
} from "react-share";

const ModalWrapper = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.95);
    top: 0;
    left: 0;
    display: grid;
    place-items: center;
    z-index: 1500;
`;
const ButtonsWrapper = styled.div`
    position: relative;
    display: grid;
    width: 80%;
    place-items: center;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 20px;
    color: black;
    z-index: 130;
    pointer-events: all;
`;
export default observer(function ModalShare({ show, setShow }) {
    if (!show) return null;
    const { selectedLending } = LandingsStore;
    const { shareTitle } = selectedLending;
    const { shareQuota } = selectedLending;
    const { shareUrl } = selectedLending;
    const { shareHashtags } = selectedLending;
    const tagsArray = shareHashtags.split("#");
    return (
        <ModalWrapper
            onClick={(e) => {
                e.stopPropagation();
            }}
        >
            <ButtonsWrapper>
                <div>
                    <br />
                    <br />
                    <br />
                </div>
                <div></div>
                <div onClick={() => setShow(false)}>
                    <CancelIcon />
                </div>

                <div>
                    <FacebookShareButton
                        quote={`${shareTitle} | ${shareQuota}`}
                        hashtag={shareHashtags}
                        url={shareUrl}
                    >
                        <FacebookIcon size={32} round />
                    </FacebookShareButton>
                </div>
                <div>
                    <EmailShareButton
                        subject={shareTitle}
                        body={`${shareQuota} ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <EmailIcon size={32} round />
                    </EmailShareButton>
                </div>
                <div>
                    <LinkedinShareButton
                        title={shareTitle}
                        summary={`${shareQuota} ${shareHashtags}`}
                        source={"Candy Landing"}
                        url={shareUrl}
                    >
                        <LinkedinIcon size={32} round />
                    </LinkedinShareButton>
                </div>
                <div>
                    <MailruShareButton
                        title={shareTitle}
                        description={`${shareQuota} ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <MailruIcon size={32} round />
                    </MailruShareButton>
                </div>
                <div>
                    <OKShareButton
                        title={shareTitle}
                        description={`${shareQuota} ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <OKIcon size={32} round />
                    </OKShareButton>
                </div>
                <div>
                    <RedditShareButton
                        title={shareTitle}
                        // description={`${shareQuota} ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <RedditIcon size={32} round />
                    </RedditShareButton>
                </div>
                <div>
                    <TelegramShareButton
                        title={`${shareTitle} | ${shareQuota} | ${shareHashtags} | @tverdn`}
                        url={shareUrl}
                    >
                        <TelegramIcon size={32} round />
                    </TelegramShareButton>
                </div>
                <div>
                    <TumblrShareButton
                        title={shareTitle}
                        tags={tagsArray}
                        caption={{ shareQuota }}
                        url={shareUrl}
                    >
                        <TumblrIcon size={32} round />
                    </TumblrShareButton>
                </div>
                <div>
                    <TwitterShareButton
                        title={shareTitle}
                        hashtags={tagsArray}
                        via={"niktverd"}
                        url={shareUrl}
                    >
                        <TwitterIcon size={32} round />
                    </TwitterShareButton>
                </div>
                <div>
                    <ViberShareButton
                        title={`${shareTitle} | ${shareQuota} | ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <ViberIcon size={32} round />
                    </ViberShareButton>
                </div>
                <div>
                    <VKShareButton
                        title={`${shareTitle} | ${shareQuota} | ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <VKIcon size={32} round />
                    </VKShareButton>
                </div>
                <div>
                    <WhatsappShareButton
                        title={`${shareTitle} | ${shareQuota} | ${shareHashtags}`}
                        url={shareUrl}
                    >
                        <WhatsappIcon size={32} round />
                    </WhatsappShareButton>
                </div>
            </ButtonsWrapper>
        </ModalWrapper>
    );
});
