import React, { useState } from "react";
import { observer } from "mobx-react-lite";
// ====================================================================================
import { Button, IconButton } from "@material-ui/core";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";
// ====================================================================================
import {
    VideoWrapper,
    // ContentViewCont,
    // ScrollWrap,
} from "../_4_wrappers/ContentView_SC.jsx";
// ====================================================================================
import SystemStore from "../../store/SystemStore.js";
// ====================================================================================
export default observer(function MessageMuted({ close }) {
    const [step, setStep] = useState(0);

    const [rus, setRus] = useState(false);
    const lang = {
        en: {
            header:
                "Facebook is limiting you from whatching video with sound on this web page",
            useDefault: "Use Chrome, Safari browser for better experience",
            unmute1: "Or try to unmute video by clicking 'Unmute' red button",
        },
        ru: {
            header:
                "Facebook ограничивает вам просмотр видео со звуком на нашем сайте.",
            useDefault:
                "Используйте (Chrome, Safari), чтобы иметь доступ ко всем функциям сайта",
            unmute1:
                "Или попробуйте включить звук нажатием на красную кнопку слева.",
        },
    };
    const choosenLang = rus ? lang.ru : lang.en;
    if (step === 0) {
        // setStep(1);
        return (
            <div
                style={{
                    position: "fixed",
                    display: "grid",
                    placeItems: "center",
                    top: 15,
                    left: 15,
                    zIndex: 100,
                }}
            >
                <IconButton
                    style={{ color: "tomato", fontSize: 2, transform: "scale(2.8)" }}
                    onClick={() => {
                        SystemStore.resetShouldBeMuted();
                    }}
                >
                    <VolumeOffIcon size="large" />
                </IconButton>
            </div>
        );
    }
    return (
        <VideoWrapper
            style={{
                zIndex: 100,
                background: "rgba(0,0,0,.90)",
                height: "85%",
                top: 0,
            }}
        >
            <h3 style={{ maxWidth: "80%", margin: 0, color: "orange" }}>
                {choosenLang.header}
            </h3>

            <div style={{ maxWidth: "80%", margin: 0 }}>
                <p style={{ color: "white" }}>
                    <b>{choosenLang.useDefault}</b>
                </p>
            </div>
            <div>
                <img
                    src={"away.gif"}
                    width="100%"
                    height="auto"
                    alt="loading..."
                />
            </div>
            <div
                style={{
                    display: "grid",
                    placeItems: "center",
                    gridTemplateColumns: "1fr 4fr",
                }}
            >
                <div>
                    <IconButton
                        style={{ color: "tomato" }}
                        onClick={() => {
                            SystemStore.resetShouldBeMuted();
                        }}
                    >
                        <VolumeOffIcon />
                    </IconButton>
                </div>
                <div>
                    <p style={{ color: "white" }}>
                        <b>{choosenLang.unmute1}</b>
                    </p>
                </div>
            </div>
            <div>
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => setRus(!rus)}
                >
                    {rus ? "En" : "Ru"}
                </Button>{" "}
                <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                        SystemStore.SetMutedDialogWasShownAndClosedByUser();
                        close(false);
                    }}
                >
                    Close
                </Button>
            </div>
        </VideoWrapper>
    );
});
