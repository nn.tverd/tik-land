import styled from "styled-components";
// *********************************************
// *********************************************
export const VideoWrap = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    display: grid;
    place-items: center;
`;

// *********************************************

// export const VideoComp = styled.video`
//     position: absolute;
//     // object-fit: fill;
//     width: 101%;
//     height: auto;
//     top: 50%;
//     transform: translateY(-50%);
//     z-index: 1;
// `;

// *********************************************

export const PlayButton = styled.div`
    position: absolute;
    z-index: 500;
    /* width: auto; */
    color: white;
    display: flex;
    height: auto;
    // top: 50%;
    // left: 50%;
    // transform: translateX(-50%);
    // transform: translateY(-50%);
`;

// *********************************************

export const VideoOverlay = styled.div`
    z-index: 2;
    position: absolute;
    width: 100%;
    height: 100%;
    // background-color: none;
    background-color: rgba(0, 0, 0, 0.1);
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    pointer-events: none;
    display: grid;
    place-items: center;
`;

// *********************************************

export const ImageHolder = styled.div`
    z-index: 499;
    position: absolute;
    width: 100%;
    height: 100%;
    // background-color: none;
    background-color: black;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    pointer-events: none;
    // display: ${(props) => (props.isHiding ? "none" : "grid")};
    display: grid;
    place-items: center;
`;
export const VideoWrapper = styled.div`
    margin-top: ${(props) => Math.round(props.offsetY__)}px;
    position: absolute;
    height: 100%;
    width: 100%;
    display: grid;
    place-items: center;
    z-index: -1;
`;