import styled from "styled-components";
// ======================================================================

export const CardComp = styled.div`
    position: relative;
    background-color: none;
    width: 100%;
    height: 100%;
    scroll-snap-align: start;
    scroll-snap-stop: always;
`;
export const ParentWrapper = styled.div`
    position: absolute;
    background-color: none;
    width: 100%;
    height: 100%;
    display: grid;
    place-items: center;
`;
