import React from "react";
import { observer } from "mobx-react-lite";
import ReactPlayer from "react-player/lazy";

// ====================================================================================
import "../css/common.css";
// ====================================================================================
// import useDebounce from "../Debounce.js";
// ====================================================================================
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// ====================================================================================
// styled components
import {
    VideoWrap,
    PlayButton,
    VideoOverlay,
    ImageHolder,
    VideoWrapper,
} from "./VideoHlsOnePlayer__SC";
// ====================================================================================
// redux actions
// import {setOnPlaceIndex} from "../../redux/ducks/ui.js"
// ====================================================================================
import VideoControlsStore from "../../store/VideoControlsStore.js";
import PagesStore from "../../store/PagesStore.js";

import SystemStore from "../../store/SystemStore.js";

// ====================================================================================

export default observer(function VideoHlsOnePlayer(props) {
    const {
        showGlobalCovers,
        globalPlayEnabled,
        setShowGlobalCovers,
        setGlobalPlayEnabled,
    } = VideoControlsStore;
    const { shownIndex } = PagesStore;
    const { page } = props;
    // const { globalPlayEnabled, showGlobalCovers } = videoControls;
    const url = page.videoUrl;
    let { coverUrl } = page;

    let coverUrl1 = "q0aqpITLNKU";
    coverUrl1 = coverUrl ? coverUrl : coverUrl1;

    if (
        url.includes("://") &&
        url.includes("youtube.com")
        // !playingStaff.firstVideoPlayForce
    ) {
        coverUrl1 = url.split("?v=")[1].split("&")[0];
        coverUrl1 = `http://img.youtube.com/vi/${coverUrl1}/0.jpg`;
    }
    const showCoverrrrrr = {
        display:
            (showGlobalCovers || !globalPlayEnabled) && SystemStore.isIOS || (shownIndex!==page._id)
                ? "grid"
                : "none",
    };

    const imgStyle = {
        position: "absolute",
        height: "120%",
        width: "auto",
    };

    const VideoComponent = (
        <VideoWrapper>
            <ReactPlayer
                width="100%"
                height="100%"
                url={page.videoUrl}
                controls={false}
                loop={true}
                playsinline={true}
                playing={globalPlayEnabled && (shownIndex===page._id)}
                onPlay={() => setShowGlobalCovers(false)}
                onPause={() => setShowGlobalCovers(true)}
                config={{
                    youtube: {
                        playerVars: {
                            rel: 0,
                            iv_load_policy: 3,
                            modestbranding: 1,
                            showinfo: 0,
                        },
                    },
                }}
            />
        </VideoWrapper>
    );

    return (
        <VideoWrap
            onClick={() => {
                setGlobalPlayEnabled(!globalPlayEnabled);
                // setShowGlobalCovers(!showGlobalCovers);
                // setGlobalPlayEnabled(!globalPlayEnabled);
            }}
        >
            {!SystemStore.isIOS && VideoComponent}
            <VideoOverlay>
                <ImageHolder style={showCoverrrrrr} isHiding={showGlobalCovers}>
                    <img style={imgStyle} src={coverUrl1} alt="" />
                </ImageHolder>
            </VideoOverlay>

            {!globalPlayEnabled && (
                <PlayButton>
                    <PlayArrowIcon
                        fontSize="large"
                        className="svg_icons_play"
                    />
                </PlayButton>
            )}
        </VideoWrap>
    );
});
