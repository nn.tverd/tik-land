import styled from "styled-components";

export const CentralWrapper = styled.div`
    position: absolute;
    bottom: 10px;
    left: 0;
    width: 100%;
    height: 100px;
    display: grid;
    place-items: center;
    grid-template-columns: 1fr 2fr 1fr;
    color: white;
    z-index: 16;
`;

export const SideButtonWrapDiv = styled.div`
    display: grid;
    justify-content: center;
    align-content: end;
    height: 100%;
    width: 100%;
    // background-color: tomato;
    border: 0px;
    padding: 0;
    text-align: center;
    font-size: 0.8em;
    z-index: 16;
    pointer-events: all;

`;

export const CentralButtonWrapDiv = styled(SideButtonWrapDiv)`
    display: grid;
    justify-content: center;
    align-content: center;
    height: 100%;
    width: 100%;
    // background-color: tomato;
    border: 0px;
    padding: 0;
    text-align: center;
    font-size: 0.8em;
    z-index: 16;
    pointer-events: all;
`;
export const CentralButtonBaseWrapDiv = styled(SideButtonWrapDiv)`
    display: grid;
    justify-content: center;
    align-content: center;
    height: 100%;
    width: 100%;
    border: 0px;
    padding: 0;
    text-align: center;
`;

export const CentralIcon = styled.div`
    position: absolute;
    width: 90px;
    height: 90px;
    display: grid;
    justify-content: center;
    align-content: center;
    text-transform: uppercase;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
`;
export const SideIcon = styled(CentralIcon)`
    background-color: transparent;
    // border: 1px solid white;
    color: white;
`;

export const AnimationWrapper = styled.div`
    position: absolute;
    bottom: 10px;
    display: block-inline;
    width: 90px;
    height: 90px;
    // padding: 0 auto;
    // border: 1px solid green;
`;

export const AnimationBox = styled.div`
    width: 90px;
    height: 90px;
    // background-color: tomato;
`;

export const CentralButtonWrapDivResizeble = styled.div`
    position: fixed;
    // background-color: orange;
    // border: 1px solid orange;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    max-width: 100%;
    max-height: 100%;
    width: 75px;
    // width: 100%;
    height: 100px;
`;

export const CentralButtonWrap = styled.button`
    font-size: 9px;
    position: absolute;
    left: 50%;
    bottom: 0;
    // transform: translateY(-010%);
    z-index: 20;
    transform: translateX(-50%);
    height: 75px;
    // width: 100%;
    width: 75px;
    color: ${(props) => (props.buttonType === 2 ? "darkslateblue" : "white")};
    // color: darkslateblue;
    background-color: rgba(0, 0, 0, 0);
    border: 0px;
    padding: 0;
    // border: 1px solid white;
    // border-radius: 5%;
    text-decoration: none;
    text-transform: uppercase;
    // color: white;
    font-size: 7px;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
    outline: none !important;
`;
export const MainButtonsFlex = styled.div`
    position: relative;
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    // background-color: tomato;
`;
export const BaseButtonWrap = styled.button`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;

    justify-content: center;
    align-items: center;
    margin: 0;
    background-color: rgba(0, 0, 0, 0);
    border: 0px;
    padding: 0;
    // border: 1px solid white;
    // border-radius: 5%;
    text-decoration: none;
    text-transform: uppercase;
    color: white;
    font-size: 7px;
    text-align: center;
    user-select: none;
    -webkit-user-select: none;
    outline: none !important;
`;
