import React from "react";
import { observer } from "mobx-react-lite";
//==============================================================================
// import styled from "styled-components";
// import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import ErrorRoundedIcon from "@material-ui/icons/ErrorRounded";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import PlaylistAddCheckIcon from "@material-ui/icons/PlaylistAddCheck";
//==============================================================================
// import { useSelector } from "react-redux";
import "../css/animations.css";
//==============================================================================
import PagesStore from "../../store/PagesStore.js";
//==============================================================================

import {
    CentralWrapper,
    // MainButtonsFlex,
    SideButtonWrapDiv,
    CentralButtonWrapDiv,
    CentralIcon,
    // BaseButtonWrap,
    // CentralButtonWrap,
    // SideIcon,
    AnimationWrapper,
    // CentralButtonWrapDivResizeble,
    CentralButtonBaseWrapDiv,
    AnimationBox,
} from "./MainAction__SC.jsx";

export default observer(function MainAction(props) {
    const { pages, shownIndex } = PagesStore;
    const { _page } = props;
    // const { pages } = props;
    let page = null;
    if (!_page) {
        if (!pages.length) {
            return null;
        }
        // const shownIndex = 1;
        page = pages[shownIndex];
    } else {
        page = _page;
    }
    // const Icon = page.iconForSidebar;
    // let ShowBlock = null;

    // if (page.showSliderHint && page.buttonType === 0) {
    //     ShowBlock = <div className="slider__down"></div>;
    // }

    const handleGoAway = (e) => {
        // Main action side button link
        if (window.confirm(page.rightButtonText)) {
            // Save it!
            // // console.log('Thing was saved to the database.');
            window.open(page.rightButtonLink, "_blank");
        } else {
            // Do nothing!
            // console.log("User quit redirection");
        }
    };
    return (
        <CentralWrapper>
            <SideButtonWrapDiv>
                {page.isLeftButtonVisible && (
                    <div
                        onClick={(e) => {
                            e.stopPropagation();

                            alert(page.leftButtonText);
                        }}
                    >
                        <div>
                            <MenuBookIcon size="small" />
                        </div>
                        {/* <div>{page.pageName} 123</div> */}
                    </div>
                )}
            </SideButtonWrapDiv>
            <CentralButtonWrapDiv>
                {page.isCentralButtonVisible && (
                    <CentralButtonBaseWrapDiv>
                        {page.centralButtonType === "arrow" ? (
                            <div className="slider__down"></div>
                        ) : (
                            <AnimationBox
                                onClick={(e) => {
                                    e.stopPropagation();
                                    window.open(
                                        page.centralButtonLink,
                                        "_blank"
                                        // "_system"
                                    );
                                }}
                            >
                                <CentralIcon
                                    style={{
                                        color:
                                            page.centralButtonType === "solid"
                                                ? "black"
                                                : "white",
                                    }}
                                >
                                    <div style={{ marginTop: -10 }}>
                                        <ErrorRoundedIcon />
                                        <div>{page.centralButtonText}</div>
                                    </div>
                                </CentralIcon>
                                <AnimationWrapper>
                                    <div
                                        className={`circle-spin ${
                                            page.centralButtonType === "solid"
                                                ? "superButtonStyle"
                                                : ""
                                        }`}
                                    ></div>
                                </AnimationWrapper>
                            </AnimationBox>
                        )}
                    </CentralButtonBaseWrapDiv>
                )}
            </CentralButtonWrapDiv>
            <SideButtonWrapDiv>
                {page.isRightButtonVisible && (
                    <div
                        onClick={(e) => {
                            e.stopPropagation();
                            // window.open(page.linkFromSideBar, "_blank");
                            handleGoAway(e);
                        }}
                    >
                        <div>
                            <PlaylistAddCheckIcon size="small" />
                        </div>
                        {/* <div>{page.pageName} 123</div> */}
                    </div>
                )}
            </SideButtonWrapDiv>
        </CentralWrapper>
    );
});
