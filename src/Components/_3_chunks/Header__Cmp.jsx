import React from "react";
// ====================================================================================
import "../css/animations.css";
// ====================================================================================
// ====================================================================================

export default function HeaderComponent(props) {
    const { page } = props;
    // const handleWaypointLeave = () => {};

    return (
        <div className="sheet">
            {page.type === "img" && page.imgUrl && (
                <img className="coverImage" src={page.imgUrl} alt={""} />
            )}

            {page.type === "img" && (
                <div className="sheet__border">
                    <div className="sheet__content">
                        <h1 className="tiktok-glithc-h1">{page.header1}</h1>
                        <h2 className="tiktok-h2">{page.header2}</h2>
                    </div>
                </div>
            )}
        </div>
    );
}
