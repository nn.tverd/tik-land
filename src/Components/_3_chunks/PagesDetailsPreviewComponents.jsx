import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";

import styled from "styled-components";
import InnerHTML from "dangerously-set-html-content";
// ********************************************************************************************
import PlayArrowRoundedIcon from "@material-ui/icons/PlayArrowRounded";
// ********************************************************************************************
// import AdminAuthStore from "../../../../store/AdminAuthStore.js";
import LandingsStore from "../../store/LandingsStore.js";
import SystemStore from "../../store/SystemStore.js";
import VideoControlsStore from "../../store/VideoControlsStore.js";
// import AdminPagesStore from "../../store/AdminPagesStore.js";
// import DebuggerStore from "../../store/DebuggerStore.js";

// ********************************************************************************************
import Footer from "../_2_molecules/Footer__Cmp.jsx";
import Sidebar from "../_2_molecules/Sidebar__Cmp.jsx";
import MainAction from "./MainAction__Cmp.jsx";
// ********************************************************************************************
import "../css/animations.css";
import PagesStore from "../../store/PagesStore.js";
import Player from "../_2_molecules/Player.jsx";
import { IconButton } from "@material-ui/core";
// ********************************************************************************************

export const CoomponentWrapper = styled.div`
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: ${(props) => (props.zInd ? props.zInd : 1)};
    overflow: hidden;
    // background-color: orange;
    display: grid;
    place-items: center;
    // pointer-events: none;
`;
// ********************************************************************************************
export const PagesDetailsPreview_VideoCoverLayout = observer(
    ({ page, showGlobalCovers }) => {
        // if (showGlobalCovers) alert("showGlobalCovers");
        // console.log(page.coverUrl);
        return (
            <CoomponentWrapper zInd={"auto"}>
                {showGlobalCovers && (
                    <div
                        style={{
                            backgroundImage: `url("${page.coverUrl}")`,
                            backgroundSize: "cover",
                            backgroundRepeat: "no-repeat",
                            backgroundPosition: "center",
                            // backgroundColor: "tomato",
                            height: "100%",
                            width: "100%",
                            display: "grid",
                            placeItems: "center",
                            pointerEvents: "all",
                            zIndex: 15,
                        }}
                    >
                        <IconButton
                            onClick={() => {
                                console.log("button clicked");
                            }}
                        >
                            <PlayArrowRoundedIcon
                                size="large"
                                style={{ color: "white", transform:"scale(6)" }}
                                className="svg_icons_play"
                            />
                        </IconButton>
                    </div>
                )}
            </CoomponentWrapper>
        );
    }
);
// ********************************************************************************************
export const PagesDetailsPreview_ReactPlayerLayout = observer(
    ({
        page,
        globalPlayEnabled,

        setShowGlobalCovers,
        setCanShowMutedMessage,
    }) => {
        // useEffect(() => {
        // }, [globalPlayEnabled, shownIndex]);
        // const [muted, setMuted] = useState(true);
        // const { shownIndex } = PagesStore;
        // const [Paused, setPaused] = useState(3);
        // const [testPlaying, setTestPlaying] = useState(false);
        const {
            setCanBeUnmuted,
            setControlCanBeUnmuted,
            setControlCanBeUnmutedTimer,
            setMuted,
            muted,
            controlCanBeUnmuted,
            controlCanBeUnmutedTimer,
            canBeUnmuted,
        } = VideoControlsStore;
        return (
            <CoomponentWrapper zInd={"auto"}>
                <Player
                    videoUrl={page.videoUrl}
                    paused={page.paused}
                    setCanShowMutedMessage={setCanShowMutedMessage}
                    setShowGlobalCovers={setShowGlobalCovers}
                    muted={SystemStore.shouldBeMuted}
                    page={page}
                    checkUnmuteObj={{
                        muted: muted,
                        controlCanBeUnmuted: controlCanBeUnmuted,
                        controlCanBeUnmutedTimer: controlCanBeUnmutedTimer,
                        canBeUnmuted: canBeUnmuted,
                        setCanBeUnmuted: setCanBeUnmuted,
                        setControlCanBeUnmuted: setControlCanBeUnmuted,
                        setControlCanBeUnmutedTimer: setControlCanBeUnmutedTimer,
                        setMuted: setMuted,
                    }}
                />
            </CoomponentWrapper>
        );
    }
);
// ********************************************************************************************
export const PagesDetailsPreview_ImageLayout = observer(({ page }) => {
    // console.log(globalPlayEnabled);
    return (
        <CoomponentWrapper zInd={"auto"}>
            <div
                style={{
                    backgroundImage: `url("${page.imgUrl}")`,
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "center",
                    height: "100%",
                    width: "100%",
                }}
            ></div>
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
export const PagesDetailsPreview_BackgroundLayout = observer(({ page }) => {
    return (
        <CoomponentWrapper zInd={"auto"}>
            <div
                style={{
                    background: `${page.bgColor}`,
                    height: "100%",
                    width: "100%",
                }}
            ></div>
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
export const PagesDetailsPreview_HTMLLayout = observer(({ page }) => {
    return (
        <CoomponentWrapper zInd={"auto"}>
            <InnerHTML
                style={{
                    height: "100%",
                    width: "100%",
                    zIndex: 14,
                    position: "relative",
                    pointerEvents: "all",
                }}
                html={page.html}
            ></InnerHTML>
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
export const PagesDetailsPreview_TextLayout = observer(({ page }) => {
    return (
        <CoomponentWrapper zInd={"auto"}>
            <div
                style={{
                    width: "100%",
                    height: "100%",
                    display: "grid",
                    placeItems: "center",
                    gridAutoRows: `${page.text1Height}fr ${page.text2Height}fr ${page.text3Height}fr`,
                    zIndex: 17,
                    pointerEvents: "none",
                }}
            >
                <PagesDetailsPreview_TextFit100
                    fitText={page.header1}
                    showBG={page.showTextSections}
                    bg="lightpink"
                    fSize={page.text1FontSize}
                    glitch={page.text1UseGlitch}
                    fontColor={page.text1FontColor}
                />
                <PagesDetailsPreview_TextFit100
                    fitText={page.header2}
                    showBG={page.showTextSections}
                    bg="lightblue"
                    fSize={page.text2FontSize}
                    glitch={page.text2UseGlitch}
                    fontColor={page.text2FontColor}
                />
                <PagesDetailsPreview_TextFit100
                    fitText={page.header3}
                    showBG={page.showTextSections}
                    bg="lightgreen"
                    fSize={page.text3FontSize}
                    glitch={page.text3UseGlitch}
                    fontColor={page.text3FontColor}
                />
            </div>
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
// ********************************************************************************************
export const PagesDetailsPreview_TextFit100 = observer(
    ({ fitText, showBG, bg, fSize, glitch, fontColor }) => {
        const bgColor = showBG ? bg : "transparent";
        const textClassName = glitch ? "tiktok-glithc-h1" : "";
        return (
            <div
                style={{
                    width: "100%",
                    height: "100%",
                    padding: 10,
                    display: "grid",
                    placeItems: "center",
                    textAlign: "center",
                    fontSize: `${fSize}em`,
                    overflow: "hidden",
                    backgroundColor: bgColor,
                    color: fontColor,
                    whiteSpace: "pre-wrap",
                }}
            >
                <b className={textClassName}>{fitText}</b>
            </div>
        );
    }
);
// ********************************************************************************************
export const PagesDetailsPreview_FooterLayout = observer(({ page }) => {
    return (
        <CoomponentWrapper zInd={"auto"}>
            <Footer page={page} />
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
export const PagesDetailsPreview_SidebarLayout = observer(
    ({ page, updateLikes, updateShares }) => {
        // console.log(
        //     LandingsStore.selectedLending.avatarUrl,
        //     LandingsStore.selectedLending.isPromoted,
        //     LandingsStore
        // );
        return (
            <CoomponentWrapper zInd={"auto"}>
                <Sidebar
                    page={page}
                    isVerified={LandingsStore.selectedLending.isPromoted}
                    avatarUrl={LandingsStore.selectedLending.avatarUrl}
                    shareTitle={LandingsStore.selectedLending.shareTitle}
                    shareUrl={LandingsStore.selectedLending.shareUrl}
                    updateLikes={updateLikes}
                    updateShares={updateShares}
                />
            </CoomponentWrapper>
        );
    }
);
// ********************************************************************************************
export const PagesDetailsPreview_MainActionLayout = observer(({ page }) => {
    return (
        <CoomponentWrapper zInd={"auto"}>
            <MainAction _page={page} />
        </CoomponentWrapper>
    );
});
// ********************************************************************************************
