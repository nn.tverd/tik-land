import React, { useState, useEffect } from "react";
import { observer } from "mobx-react-lite";
// import { Waypoint } from "react-waypoint";
// import styled from "styled-components";
import VisibilitySensor from "react-visibility-sensor";
// ====================================================================================
// styled components
import { CardComp } from "./Card__SC.jsx";
// ====================================================================================
// import useDebounce from "../Debounce.js";
// ====================================================================================
// redux actions
// ====================================================================================
// import "./Video/Video.css";
// ====================================================================================
// import Footer from "../_2_molecules/Footer__Cmp.jsx";
// import Sidebar from "../_2_molecules/Sidebar__Cmp.jsx";
// // import TopAlert from "../_2_molecules/TopAlert__Cmp.jsx";
// import HeaderComponent from "./Header__Cmp.jsx";
// import VideoHlsOnePlayer from "./VideoHlsOnePlayer__Cmp.jsx";
// ====================================================================================
import PagesStore from "../../store/PagesStore.js";
import SystemStore from "../../store/SystemStore.js";

import VideoControlsStore from "../../store/VideoControlsStore.js";
import {
    PagesDetailsPreview_VideoCoverLayout,
    PagesDetailsPreview_BackgroundLayout,
    PagesDetailsPreview_ReactPlayerLayout,
    PagesDetailsPreview_ImageLayout,
    PagesDetailsPreview_HTMLLayout,
    PagesDetailsPreview_TextLayout,
    PagesDetailsPreview_FooterLayout,
    PagesDetailsPreview_SidebarLayout,
    PagesDetailsPreview_MainActionLayout,
} from "./PagesDetailsPreviewComponents.jsx";

//==============================================================================

export default observer(function CardComponent(props) {
    const {
        page,
        setCanShowMutedMessage,
        // ref,
        // handleAutoScrollOnLoading,
    } = props;

    // console.log(page.isActive);
    const { setShownIndex, shownIndex, updateLikes, updateShares } = PagesStore;
    const {
        showGlobalCovers,
        setShowGlobalCovers,
        globalPlayEnabled,
        setGlobalPlayEnabled,
    } = VideoControlsStore;
    const [holdCover, setHoldCover] = useState(false);
    const [timeOutId, setTimeOutId] = useState(null);

    useEffect(() => {
        // console.log(
        //     "useEffect - holdCover",
        //     holdCover,
        //     showGlobalCovers,
        //     globalPlayEnabled
        // );
        clearTimeout(timeOutId);
        if (!holdCover && showGlobalCovers && globalPlayEnabled) {
            const id = setTimeout(() => {
                setShowGlobalCovers(false);
            }, 500);
            setTimeOutId(id);
            return () => {
                clearTimeout(id);
            };
        } else {
            setTimeOutId(null);
        }
    }, [holdCover]);
    // const [_shownIndex, _setShownIndex] = useState(false);
    function onChange(isVisible) {
        if (!isVisible) {
            const interval = 100;
            let count = 0;
            setHoldCover(true);
            setTimeout(function xRun() {
                if (count++ > 10) return;
                setHoldCover(true);
                setTimeout(xRun, interval);
            }, interval);
            return;
        }
        setHoldCover(false);
        setShownIndex(page._id);
        // setFirstPlay(true);
    }

    // console.log("page.empty =", page.empty);
    // if (page.empty) handleAutoScrollOnLoading(shownIndex);
    return (
        <VisibilitySensor
            intervalCheck={true}
            intervalDelay={145}
            offset={{ top: -100, bottom: -100, left: -100, right: -100 }}
            scrollCheck={false}
            scrollDelay={50}
            onChange={onChange}
        >
            {page.empty ? (
                <CardComp>loading...</CardComp>
            ) : (
                <CardComp
                    // ref={ref}
                    onClick={() => {
                        setGlobalPlayEnabled(!globalPlayEnabled);
                        setShowGlobalCovers(!showGlobalCovers);
                        PagesStore.setAllPagesPaused(shownIndex);
                    }}
                    onTouchStart={() => {
                        console.log("onTouchMove");
                        if (SystemStore.isIOS) {
                            setShowGlobalCovers(true);
                            setHoldCover(true);
                        }
                    }}
                    onTouchMove={() => {
                        console.log("onTouchMove");
                        if (SystemStore.isIOS) {
                            setShowGlobalCovers(true);
                            setHoldCover(true);
                        }
                    }}
                    onTouchEnd={() => {
                        console.log("onTouchEnd");
                        setHoldCover(false);
                    }}
                    // ref={refs[page._id]}
                >
                    {page.type === "video" && !SystemStore.isIOS && (
                        <PagesDetailsPreview_ReactPlayerLayout
                            page={page}
                            globalPlayEnabled={globalPlayEnabled}
                            shownIndex={shownIndex}
                            setShowGlobalCovers={setShowGlobalCovers}
                            setCanShowMutedMessage={setCanShowMutedMessage}
                        />
                    )}

                    {page.type === "video" && (
                        <PagesDetailsPreview_VideoCoverLayout
                            page={page}
                            // showGlobalCovers={showGlobalCovers}
                            showGlobalCovers={!!page.paused}
                        />
                    )}
                    {page.type === "img" && (
                        <PagesDetailsPreview_ImageLayout page={page} />
                    )}
                    {page.type === "bg" && (
                        <PagesDetailsPreview_BackgroundLayout page={page} />
                    )}
                    {page.type === "html" && (
                        <PagesDetailsPreview_HTMLLayout page={page} />
                    )}
                    {<PagesDetailsPreview_TextLayout page={page} />}
                    {<PagesDetailsPreview_FooterLayout page={page} />}
                    {
                        <PagesDetailsPreview_SidebarLayout
                            page={page}
                            updateLikes={updateLikes}
                            updateShares={updateShares}
                        />
                    }

                    {<PagesDetailsPreview_MainActionLayout page={page} />}
                </CardComp>
            )}
        </VisibilitySensor>
    );
});
